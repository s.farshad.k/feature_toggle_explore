import pickle
import subprocess
from time import sleep
import numpy as np 
f = open('records.pckl', 'rb')
records=pickle.load(f);
# exit()
f = open('repos.pckl', 'rb')
repos=pickle.load(f);
# print(len(repos))
try:
    f = open('statistics1.pckl', 'rb')
    repo_info=pickle.load(f);
except:
    repo_info={}
initial_info_size=len(repo_info.keys())

record_by_repo = {}
for key in records.keys():
    for record in records[key]:
        record_by_repo[record['repo']] = record

repos=np.unique(repos)
i=0
remained_repos=[]

if len(repo_info):
    last_repo = list(repo_info.keys()).pop();
    processed=False
    for repo in repos:
        if processed:
            remained_repos.append(repo)
        if(repo==last_repo): #it means we have already processed these repos
            processed=True
else:
    remained_repos=repos
# print(len(remained_repos))
# exit()
for repo in remained_repos:
    try:
        i+=1
        print(str(i)+"Current Repo:"+ repo)
        folder_name=repo.replace("/",'_')
        try:
            batcmd="mkdir  ./repos/{}".format(folder_name,folder_name)
            init = subprocess.check_output(batcmd, shell=True)
        except:
            print("error mkdir");
            sleep(1)
        subprocess.check_output("git init ./repos/{} ".format(folder_name), shell=True)
        subprocess.check_output("git --git-dir=./repos/{}/.git remote add origin https://github.com/{}".format(folder_name,repo), shell=True)
        subprocess.check_output("git --git-dir=./repos/{}/.git fetch ".format(folder_name), shell=True)
        branches=subprocess.check_output('git --git-dir=./repos/{}/.git remote show origin | grep "HEAD branch" | cut -d ":" -f 2'.format(folder_name), shell=True).decode("utf-8")
        print(str(branches).strip())
        batcmd="git --git-dir=./repos/{}/.git log origin/{} --pretty=oneline --abbrev-commit".format(folder_name,str(branches).strip())
        print(batcmd)
        commits = subprocess.check_output(batcmd, shell=True).decode("utf-8")
        # print("commits:"+(commits.decode("utf-8")))
        commitsNo = subprocess.check_output("git --git-dir=./repos/{}/.git log origin/{} --pretty=oneline --abbrev-commit | wc -l".format(folder_name,str(branches).strip()), shell=True).decode("utf-8")
        # print("commitsNo:"+(commitsNo.decode("utf-8")))
        batcmd="svn ls -R https://github.com/{}/branches/master |grep -v '/$' |grep -v '^\.' | wc -l".format(repo)
        filesNo = subprocess.check_output(batcmd, shell=True).decode("utf-8")
        repo_info[repo] = {
            'record':record_by_repo[repo],
            'filesNo':filesNo,
            'commits':commits,
            'commitsNo':commitsNo,
        }
        print(repo_info[repo])
        if i%5==0:
            f = open('./statistics.pckl', 'wb')
            pickle.dump(repo_info, f)
            f.close()
            print('saved result till'+str(i))

    except Exception as e:
        # print(//"hi"+str(e)); 
        f = open('./repos/error-'+repo.replace('/','_')+'.pckl', 'wb')
        pickle.dump(str(e), f)
        f.close()
        print("error: "+str(e)+" in repo: "+repo)
        sleep(10);
        continue;
    print('total till now:'+str(len(repo_info.keys())))
    sleep(0.1);
if len(repo_info.keys())==initial_info_size:
    exit()
f = open('./statistics.pckl', 'wb')
pickle.dump(repo_info, f)
f.close()


# git log origin/master --pretty=format:"%H" --abbrev-commit
# repo=repos[0]
# batcmd="git init /repos/ | mkdir  ./repos/{} ".format(repo.replace("/",'_'))
# print(batcmd)
# result = subprocess.check_output(batcmd, shell=True)
# print(result)