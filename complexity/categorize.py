import numpy as np
import pandas as pd
import re
import io
import os 

# df = pd.read_excel (r'')
# print (df)
xls = pd.ExcelFile('./complexity/toggles.xlsx')
df1 = pd.read_excel(xls, 'life-cycle')
#---------------------------------
#detect language
# i=0
# ext = df1.iloc[i]['mod1 file'].split('.').pop().lower()

# if(ext=='jsx' or ext='tsx'):
# code_chunk=df1.iloc[i]['mod1'].strip()
# regexp = re.compile(r"<[A-Z]")
# if regexp.search(code_chunk):
#     lang='jsx'
# elif (ext=='html' or ext=='htm'): #it is angular
#     lang='angular'
# elif (ext=='hbs'):
#     lang='hbs'
# elif (ext=='ts'):
#     lang='ts'
# else:
#     lang='js'
# elif (ext=='t'):
print(len(df1))
extensions={};
for i in range(0,len(df1)):   
    print(i)
    # print(df1.iloc[i]['mod1 file'])
    ext = df1.iloc[i]['mod1 file'].split('.').pop().lower().strip()
    code_chunk=df1.iloc[i]['mod1'].strip()
    regexp = re.compile(r"<[A-Z]")
    if regexp.search(code_chunk):
        lang='jsx'
    elif (ext=='html' or ext=='htm'): #it is angular
        lang='angular'
    elif (ext=='hbs'):
        lang='hbs'
    elif (ext=='ts' or ext=='tsx'):
        lang='ts'
    elif (ext=='vue'):
        lang='ts'
    else:
        lang='js'
    # extensions.append(ext)
    if lang in extensions.keys():
        extensions[lang]+=1
    else:
        os.mkdir('./complexity/files/{}'.format(lang)) 
        extensions[lang]=1
    name=str(i)+'.'+ext
    content = df1.iloc[i]['mod1'].strip()
    with io.open('./complexity/files/{}/{}.{}'.format(lang,str(i),ext), "w", encoding="utf-8") as f:
        f.write(content)

# extensions = np.unique(extensions)
print(extensions)
# print(df1.iloc[0]['mod1'])
# df2 = pd.read_excel(xls, 'Sheet2')