React.createElement(
                                "div",
                                null,
                                "Bullet Train lets you manage ",
                                explain ? React.createElement(
                                                                Link,
                                                                { to: "/blog/remote-config-and-feature-flags" },
                                                                "feature flags"
                                ) : "feature flags",
                                " and ",
                                explain ? React.createElement(
                                                                Link,
                                                                { to: "/blog/remote-config-and-feature-flags" },
                                                                "remote config"
                                ) : "remote config"
);