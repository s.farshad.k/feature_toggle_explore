if (this.features.isEnabled('editor-html-paste')) {
      try {
         const inputParser = new HTMLInputParser({});
         const htmlPaste = (event.clipboardData || window.clipboardData).getData('text/html');
         const cleanHTML = inputParser.cleanupHTML(htmlPaste);
         const sel = this.rawEditor.selectHighlight(this.rawEditor.currentSelection);
         this.rawEditor.update(sel, {set: { innerHTML: cleanHTML}});
      }
      catch(e) {
        // fall back to text pasting
        console.warn(e);
        const text = (event.clipboardData || window.clipboardData).getData('text');
        const sel = this.rawEditor.selectHighlight(this.rawEditor.currentSelection);
        this.rawEditor.update(sel, {set: { innerHTML: text}});
      }
     }