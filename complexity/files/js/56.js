let hasSPD = this.get('complianceStatus.authorizationContext.enabledFeatures.spd');

    if(!hasSPD) {
      this.transitionTo('gridiron-admin');
    }