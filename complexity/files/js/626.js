if (this.get('features.github-apps')) {
        hashObject.lockedGithubAppsRepositories = this.store.paginated(
          'repo',
          githubActiveOnOrgParams,
          { live: false }
        );

        hashObject.notLockedGithubAppsRepositories = this.store.paginated(
          'repo',
          githubInactiveOnOrgParams,
          { live: false }
        );
      }