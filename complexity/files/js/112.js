if (isEnabled('ds-improved-ajax')) {

  RESTAdapter.reopen({

    /**
     * Get the data (body or query params) for a request.
     *
     * @public
     * @method dataForRequest
     * @param {Object} params
     * @return {Object} data
     */
    dataForRequest(params) {
      var { store, type, snapshot, requestType, query } = params;

      // type is not passed to findBelongsTo and findHasMany
      type = type || (snapshot && snapshot.type);

      var serializer = store.serializerFor(type.modelName);
      var data = {};

      switch (requestType) {
        case 'createRecord':
          serializer.serializeIntoHash(data, type, snapshot, { includeId: true });
          break;

        case 'updateRecord':
          serializer.serializeIntoHash(data, type, snapshot);
          break;

        case 'findRecord':
          data = this.buildQuery(snapshot);
          break;

        case 'findAll':
          if (params.sinceToken) {
            query = query || {};
            query.since = params.sinceToken;
          }
          data = query;
          break;

        case 'query':
        case 'queryRecord':
          if (this.sortQueryParams) {
            query = this.sortQueryParams(query);
          }
          data = query;
          break;

        case 'findMany':
          data = { ids: params.ids };
          break;

        default:
          data = undefined;
          break;
      }

      return data;
    },

    /**
     * Get the HTTP method for a request.
     *
     * @public
     * @method methodForRequest
     * @param {Object} params
     * @return {String} HTTP method
     */
    methodForRequest(params) {
      const { requestType } = params;

      switch (requestType) {
        case 'createRecord': return 'POST';
        case 'updateRecord': return 'PUT';
        case 'deleteRecord': return 'DELETE';
      }

      return 'GET';
    },

    /**
     * Get the URL for a request.
     *
     * @public
     * @method urlForRequest
     * @param {Object} params
     * @return {String} URL
     */
    urlForRequest(params) {
      var { type, id, ids, snapshot, snapshots, requestType, query } = params;

      // type and id are not passed from updateRecord and deleteRecord, hence they
      // are defined if not set
      type = type || (snapshot && snapshot.type);
      id = id || (snapshot && snapshot.id);

      switch (requestType) {
        case 'findAll':
          return this.buildURL(type.modelName, null, null, requestType);

        case 'query':
        case 'queryRecord':
          return this.buildURL(type.modelName, null, null, requestType, query);

        case 'findMany':
          return this.buildURL(type.modelName, ids, snapshots, requestType);

        case 'findHasMany':
        case 'findBelongsTo':
          let url = this.buildURL(type.modelName, id, null, requestType);
          return this.urlPrefix(params.url, url);
      }

      return this.buildURL(type.modelName, id, snapshot, requestType, query);
    },

    /**
     * Get the headers for a request.
     *
     * By default the value of the `headers` property of the adapter is
     * returned.
     *
     * @public
     * @method headersForRequest
     * @param {Object} params
     * @return {Object} headers
     */
    headersForRequest(params) {
      return this.get('headers');
    },

    /**
     * Get an object which contains all properties for a request which should
     * be made.
     *
     * @private
     * @method _requestFor
     * @param {Object} params
     * @return {Object} request object
     */
   _requestFor(params) {
      const method = this.methodForRequest(params);
      const url = this.urlForRequest(params);
      const headers = this.headersForRequest(params);
      const data = this.dataForRequest(params);

      return { method, url, headers, data };
    },

    /**
     * Convert a request object into a hash which can be passed to `jQuery.ajax`.
     *
     * @private
     * @method _requestToJQueryAjaxHash
     * @param {Object} request
     * @return {Object} jQuery ajax hash
     */
    _requestToJQueryAjaxHash(request) {
      const hash = {};

      hash.type = request.method;
      hash.url = request.url;
      hash.dataType = 'json';
      hash.context = this;

      if (request.data) {
        if (request.type !== 'GET') {
          hash.contentType = 'application/json; charset=utf-8';
          hash.data = JSON.stringify(request.data);
        } else {
          hash.data = request.data;
        }
      }

      var headers = request.headers;
      if (headers !== undefined) {
        hash.beforeSend = function(xhr) {
          Object.keys(headers).forEach((key) => xhr.setRequestHeader(key, headers[key]));
        };
      }

      return hash;
    },

    /**
     * Make a request using `jQuery.ajax`.
     *
     * @private
     * @method _makeRequest
     * @param {Object} request
     * @return {Promise} promise
     */
    _makeRequest(request) {
      const adapter = this;
      const hash = this._requestToJQueryAjaxHash(request);

      const { method, url } = request;
      const requestData = { method, url };

      return new Ember.RSVP.Promise((resolve, reject) => {

        hash.success = function(payload, textStatus, jqXHR) {
          let response = adapter.handleResponse(
            jqXHR.status,
            parseResponseHeaders(jqXHR.getAllResponseHeaders()),
            payload,
            requestData
          );

          if (response instanceof AdapterError) {
            Ember.run.join(null, reject, response);
          } else {
            Ember.run.join(null, resolve, response);
          }
        };

        hash.error = function(jqXHR, textStatus, errorThrown) {
          let error;

          if (errorThrown instanceof Error) {
            error = errorThrown;
          } else if (textStatus === 'timeout') {
            error = new TimeoutError();
          } else if (textStatus === 'abort') {
            error = new AbortError();
          } else {
            error = adapter.handleResponse(
              jqXHR.status,
              parseResponseHeaders(jqXHR.getAllResponseHeaders()),
              adapter.parseErrorResponse(jqXHR.responseText) || errorThrown,
              requestData
            );
          }

          Ember.run.join(null, reject, error);
        };

        adapter._ajaxRequest(hash);

      }, `DS: RESTAdapter#makeRequest: ${method} ${url}`);
    }
  });

}