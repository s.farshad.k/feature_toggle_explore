if (isEnabled("ds-payload-type-hooks")) {
      let modelName = this.modelNameFromPayloadType(resourceHash.type);
      let deprecatedModelNameLookup = this.modelNameFromPayloadKey(resourceHash.type);

      if (modelName !== deprecatedModelNameLookup && this._hasCustomModelNameFromPayloadKey()) {
        deprecate("You are using modelNameFromPayloadKey to normalize the type for a polymorphic relationship. This has been deprecated in favor of modelNameFromPayloadType", false, {
          id: 'ds.json-api-serializer.deprecated-model-name-for-polymorphic-type',
          until: '3.0.0'
        });

        modelName = deprecatedModelNameLookup;
      }

     return modelName;
    } else {
      return this.modelNameFromPayloadKey(resourceHash.type);
    }