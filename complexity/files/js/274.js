if (this.listing.transparencyAttestation && !this.isOn('effective-rule-date-plus-one-week')) {
                dev.transparencyAttestations = [{
                    acbId: this.listing.certifyingBody.id,
                    acbName: this.listing.certifyingBody.name,
                    attestation: { transparencyAttestation: this.listing.transparencyAttestation.transparencyAttestation },
                }];
             }