if (isEnabled("ds-payload-type-hooks")) {
      modelName = this.modelNameFromPayloadType(resourceHash.type);
      let deprecatedModelNameLookup = this.modelNameFromPayloadKey(resourceHash.type);

      usedLookup = 'modelNameFromPayloadType';

      if (modelName !== deprecatedModelNameLookup && this._hasCustomModelNameFromPayloadKey()) {
        deprecate("You are using modelNameFromPayloadKey to normalize the type for a resource. This has been deprecated in favor of modelNameFromPayloadType", false, {
          id: 'ds.json-api-serializer.deprecated-model-name-for-resource',
          until: '3.0.0'
        });

        modelName = deprecatedModelNameLookup;
        usedLookup = 'modelNameFromPayloadKey';
      }
    } else {
      modelName = this.modelNameFromPayloadKey(resourceHash.type);
      usedLookup = 'modelNameFromPayloadKey';
    }