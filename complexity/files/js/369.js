if (featureFlags.isOn('enhanced-reports')) {
                        reportsStates['enhanced-reports'].forEach(state => {
                            if ($uiRouter.stateRegistry.get(state.name)) {
                                $uiRouter.stateRegistry.deregister(state.name);
                           }
                            $uiRouter.stateRegistry.register(state);
                            needsReload = needsReload || $state.$current.name === state.name;
                        });
                    }