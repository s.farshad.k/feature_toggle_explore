if (isEnabled('ds-improved-ajax')) {
      const request = this._requestFor({
        store, snapshot, url, relationship,
        requestType: 'findBelongsTo'
      });

      return this._makeRequest(request);
    } else {
      var id   = snapshot.id;
      var type = snapshot.modelName;

      url = this.urlPrefix(url, this.buildURL(type, id, null, 'findBelongsTo'));
      return this.ajax(url, 'GET');
    }