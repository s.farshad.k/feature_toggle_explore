if (isEnabled('ds-references')) {

  InternalModel.prototype.referenceFor = function(type, name) {
    var reference = this.references[name];

    if (!reference) {
      var relationship = this._relationships.get(name);

      if (type === "belongsTo") {
        reference = new BelongsToReference(this.store, this, relationship);
      } else if (type === "hasMany") {
        reference = new HasManyReference(this.store, this, relationship);
      }

      this.references[name] = reference;
    }

    return reference;
  };

}