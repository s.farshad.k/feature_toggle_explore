if (isEnabled("ds-serialize-id")) {

  JSONSerializer.reopen({
    serializeId(snapshot, json, primaryKey) {
      var id = snapshot.id;

      if (id) {
        json[primaryKey] = id;
      }
    }
  });
}