if (isEnabled('ds-deprecate-store-serialize')) {
      deprecate('Use of store.serialize is deprecated, use record.serialize instead.', false, {
        id: 'ds.store.serialize',
        until: '3.0'
      });
    }