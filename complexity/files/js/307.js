if (activity.description.startsWith('Split ') && featureFlags.isOn('better-split')) {
                activity.change.push('Split Version ' + prev.version + ' into Versions ' + curr[0].version + ' and ' + curr[1].version);
                if (this.interpretedActivity.versions.indexOf(prev.id) === -1) {
                    let that = this;
                    that.interpretedActivity.versions.push(prev.id);
                    that.networkService.getSingleVersionActivityMetadata(prev.id, {end: activity.date}).then(response => {
                        let promises = response.map(item => that.networkService.getActivityById(item.id).then(response => that._interpretVersion(response)));
                        that.$q.all(promises)
                            .then(response => {
                                that.activity = that.activity
                                    .concat(response)
                                    .filter(a => a.change && a.change.length > 0);
                            });
                    });
                }
            }