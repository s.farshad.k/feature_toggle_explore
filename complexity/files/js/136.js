if (isEnabled("ds-payload-type-hooks")) {
    
      JSONAPISerializer.reopen({
    
        modelNameFromPayloadType(type) {
          return singularize(normalizeModelName(type));
        },
    
        payloadTypeFromModelName(modelName) {
          return pluralize(modelName);
        },
    
        _hasCustomModelNameFromPayloadKey() {
          return this.modelNameFromPayloadKey !== JSONAPISerializer.prototype.modelNameFromPayloadKey;
        },
    
        _hasCustomPayloadKeyFromModelName() {
          return this.payloadKeyFromModelName !== JSONAPISerializer.prototype.payloadKeyFromModelName;
        }
    
      });
    
    }