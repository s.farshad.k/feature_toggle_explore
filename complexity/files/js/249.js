if (featureFlags.isOn('ocd2749')) {
                    usersStates['ocd2749-on'].forEach(state => {
                        $uiRouter.stateRegistry.deregister(state.name);
                        $uiRouter.stateRegistry.register(state);
                        needsReload = needsReload || $state.$current.name === state.name;
                    });
                }