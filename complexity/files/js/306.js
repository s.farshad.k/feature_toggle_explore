if (activity.description.startsWith('Split ') && featureFlags.isOn('better-split')) {
                activity.change.push('Split Product ' + prev.name + ' into Products ' + curr[0].name + ' and ' + curr[1].name);
                if (this.interpretedActivity.products.indexOf(prev.id) === -1) {
                    let that = this;
                    that.interpretedActivity.products.push(prev.id);
                    that.networkService.getSingleProductActivityMetadata(prev.id, {end: activity.date}).then(response => {
                        let promises = response.map(item => that.networkService.getActivityById(item.id).then(response => that._interpretProduct(response)));
                        that.$q.all(promises)
                            .then(response => {
                                that.activity = that.activity
                                    .concat(response)
                                    .filter(a => a.change && a.change.length > 0);
                            });
                    });
                }
            }