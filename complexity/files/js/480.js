return !this.get('auth.signedIn') &&
      !this.get('features.proVersion') &&
      !this.get('landingPage');