_extractEmbeddedBelongsTo: function(store, key, embeddedTypeClass, hash) {
    if (Ember.FEATURES.isEnabled('ds-new-serializer-api') && this.get('isNewSerializerAPI')) {
      return _newExtractEmbeddedBelongsTo.apply(this, arguments);
    }