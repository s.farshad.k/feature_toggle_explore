if (isEnabled("ds-payload-type-hooks")) {
        json[typeKey] = this.payloadTypeFromModelName(belongsTo.modelName);
      } else {
        json[typeKey] = camelize(belongsTo.modelName);
      }