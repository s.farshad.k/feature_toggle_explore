{{#if complianceStatus.authorizationContext.enabledFeatures.spd}}
  <section class="compliance-section {{if complianceStatus.requiresSPD 'spd-required' 'spd-complete'}}">
    <header>
      <span class="header-title">Security Program</span>
    </header>
 
    <div class="row">
      {{#each securityProgramSteps as |step|}}
        <div class="col-xs-6">
          {{#link-to step.path class="panel-link"}}
            <div class="panel panel-default security-program-step resource-list-item">
              <div class="panel-heading">
                <h5>{{step.name}}</h5>
              </div>
              <div class="panel-body">
                {{step.description}}
              </div>
             </div>
          {{/link-to}}
        </div>
      {{/each}}
    </div>
  </section>
{{/if}}