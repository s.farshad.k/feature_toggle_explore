if (ENV.featureFlags['pro-version'] || ENV.featureFlags['enterprise-version']) {
  Pusher.channel_auth_transport = 'bulk_ajax';
  Pusher.authorizers.bulk_ajax = function (socketId, _callback) {
    var channels, name, names;
    channels = Travis.pusher.pusher.channels;
    Travis.pusher.pusherSocketId = socketId;
    channels.callbacks = channels.callbacks || [];
    name = this.channel.name;
    names = Object.keys(channels.channels);
    channels.callbacks.push(function (auths) {
      return _callback(false, {
        auth: auths[name]
      });
    });
    if (!channels.fetching) {
      channels.fetching = true;
      return TravisPusher.ajaxService.post(Pusher.channel_auth_endpoint, {
        socket_id: socketId,
        channels: names
      }, function (data) {
        var callback, i, len, ref, results;
        channels.fetching = false;
        ref = channels.callbacks;
        results = [];
        for (i = 0, len = ref.length; i < len; i++) {
          callback = ref[i];
          results.push(callback(data.channels));
        }
        return results;
      });
    }
  };