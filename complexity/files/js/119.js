if (isEnabled('ds-improved-ajax')) {
      const request = this._requestFor({
        store, type, query,
        requestType: 'queryRecord'
      });

      return this._makeRequest(request);
    } else {
      var url = this.buildURL(type.modelName, null, null, 'queryRecord', query);

      if (this.sortQueryParams) {
        query = this.sortQueryParams(query);
      }

      return this.ajax(url, 'GET', { data: query });
    }