if (!this.get('features.proVersion')) {
      transition.abort();
      this.transitionTo(`${model.isUser ? 'account' : 'organization'}.repositories`);
    }