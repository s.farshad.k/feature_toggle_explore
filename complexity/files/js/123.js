if (isEnabled('ds-improved-ajax')) {

  JSONAPIAdapter.reopen({

    methodForRequest(params) {
      if (params.requestType === 'updateRecord') {
        return 'PATCH';
      }

      return this._super(...arguments);
    },

    dataForRequest(params) {
      const { requestType, ids } = params;

      if (requestType === 'findMany') {
        return {
          filter: { id: ids.join(',') }
        };
      }

      if (requestType === 'updateRecord') {
        const { store, type, snapshot } = params;
        const data = {};
        const serializer = store.serializerFor(type.modelName);

        serializer.serializeIntoHash(data, type, snapshot, { includeId: true });

        return data;
      }

      return this._super(...arguments);
    },

    headersForRequest() {
      const headers = this._super(...arguments) || {};

      headers['Accept'] = 'application/vnd.api+json';

      return headers;
    },

    _requestToJQueryAjaxHash() {
      const hash = this._super(...arguments);

      if (hash.contentType) {
        hash.contentType = 'application/vnd.api+json';
      }

      return hash;
    }

  });

}