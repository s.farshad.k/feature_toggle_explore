if(config.featureFlags.dataEnvironments) {
            if (dataEnvironments.get('isNew') || dataEnvironments.get('document.length') === 0) {
              reject();
            }
            dataEnvironmentSelections = dataEnvironments.get('document');
           }