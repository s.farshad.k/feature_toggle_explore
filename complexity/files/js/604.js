if (this.get('features.dashboard')) {
      return this.transitionTo('dashboard');
    } else {
      return this.transitionTo('main.repositories');
    }