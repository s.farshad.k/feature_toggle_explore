{hasFeature('free_tier') ?
           <div className="text-center margin-bottom">You may want to consider upgrading to a paid plan that includes more usage.</div> :
           <div className="text-center margin-bottom">As an early adopter of Bullet Train you will be able to use this service for free until DD/MM/YYYY. You will then need to choose a payment plan to continue using Bullet Train.</div>
          }