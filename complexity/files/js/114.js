if (isEnabled('ds-improved-ajax')) {
      const request = this._requestFor({
        store, type, snapshot,
        requestType: 'updateRecord'
      });

      return this._makeRequest(request);
    } else {
      var data = {};
      var serializer = store.serializerFor(type.modelName);

      serializer.serializeIntoHash(data, type, snapshot);

      var id = snapshot.id;
      var url = this.buildURL(type.modelName, id, snapshot, 'updateRecord');

      return this.ajax(url, "PUT", { data: data });
    }