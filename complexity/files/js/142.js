if (isEnabled("ds-payload-type-hooks")) {
      let modelName = this.modelNameFromPayloadType(relationshipDataHash.type);
      let deprecatedModelNameLookup = this.modelNameFromPayloadKey(relationshipDataHash.type);

      if (modelName !== deprecatedModelNameLookup && this._hasCustomModelNameFromPayloadKey()) {
        deprecate("You are using modelNameFromPayloadKey to normalize the type for a relationship. This has been deprecated in favor of modelNameFromPayloadType", false, {
          id: 'ds.json-api-serializer.deprecated-model-name-for-relationship',
          until: '3.0.0'
        });

        modelName = deprecatedModelNameLookup;
      }

      relationshipDataHash.type = modelName;
    } else {
      let type = this.modelNameFromPayloadKey(relationshipDataHash.type);
      relationshipDataHash.type = type;
    }