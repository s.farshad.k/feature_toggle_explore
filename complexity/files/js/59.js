if(config.featureFlags.dataEnvironments) {
    securityControlGroups.push({ handle: 'aptible_security_controls',
                                 provider: 'aptible', type: 'data-environment' });
  }