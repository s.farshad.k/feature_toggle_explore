if (isEnabled("ds-payload-type-hooks")) {
            payloadType = this.payloadTypeFromModelName(item.modelName);
            let deprecatedPayloadTypeLookup = this.payloadKeyFromModelName(item.modelName);

            if (payloadType !== deprecatedPayloadTypeLookup && this._hasCustomPayloadKeyFromModelName()) {
              deprecate("You used payloadKeyFromModelName to serialize type for belongs-to relationship. Use payloadTypeFromModelName instead.", false, {
                id: 'ds.json-api-serializer.deprecated-payload-type-for-has-many',
                until: '3.0.0'
              });

              payloadType = deprecatedPayloadTypeLookup;
            }
          } else {
            payloadType = this.payloadKeyFromModelName(item.modelName);
          }