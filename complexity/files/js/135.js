if (isEnabled("ds-payload-type-hooks")) {
          let modelName = this.modelNameFromPayloadType(relationshipHash.type);
          let deprecatedModelNameLookup = this.modelNameFromPayloadKey(relationshipHash.type);

          if (modelName !== deprecatedModelNameLookup && this._hasCustomModelNameFromPayloadKey()) {
            deprecate("You used modelNameFromPayloadKey to customize how a type is normalized. Use modelNameFromPayloadType instead", false, {
              id: 'ds.json-serializer.deprecated-type-for-polymorphic-relationship',
              until: '3.0.0'
            });

            modelName = deprecatedModelNameLookup;
          }

          relationshipHash.type = modelName;

        } else {
          relationshipHash.type = this.modelNameFromPayloadKey(relationshipHash.type);
        }