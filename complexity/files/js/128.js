const extendedErrorsEnabled = isEnabled('ds-extended-errors');  if (extendedErrorsEnabled) {
    ErrorClass.extend = extendFn(ErrorClass);
  }