if (featureFlags.isOn('complaints') && featureFlags.isOn('surveillance-reporting')) {
            surveillanceStates['complaints-on-and-surveillance-reports-on'].forEach(state => {
                $uiRouter.stateRegistry.register(state);
            });
        }