if (isEnabled('ds-improved-ajax')) {
      const request = this._requestFor({
        store, type, id, snapshot,
        requestType: 'findRecord'
      });

      return this._makeRequest(request);
    } else {
      const url = this.buildURL(type.modelName, id, snapshot, 'findRecord');
      const query = this.buildQuery(snapshot);

      return this.ajax(url, 'GET', { data: query });
    }