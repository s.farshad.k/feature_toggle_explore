if (featureFlags.isOn(COMPONENT.FEATURE_FLAGS.FLAG_PRESENT_PROVIDER_NETWORKS_ASSOCIATIONS)) {
                var url = COMPONENT.AAI_GET_PROVIDER_NETWORKS_ASSOCIATIONS + '?'
                    + 'globalCustomerId=' + globalCustomerId
                    + '&serviceType=' + serviceType
                    + '&serviceInstanceId=' + serviceInstanceId
                    + '&sdcModelUuid=' + sdcModelUuid
                ;

                $http.get(url).then(function(res){
                    defer.resolve(res.data);
                }).catch(function(err) {
                    $log.error(err);
                    defer.resolve({});
                });

            }