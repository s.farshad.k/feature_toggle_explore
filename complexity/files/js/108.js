if (isEnabled('ds-pushpayload-return')) {
      return this._adapterRun(() => { return serializer.pushPayload(this, payload); });
    } else {
      this._adapterRun(() => serializer.pushPayload(this, payload));
    }