if (isEnabled("ds-payload-type-hooks")) {
      payloadType = this.payloadTypeFromModelName(snapshot.modelName);
      let deprecatedPayloadTypeLookup = this.payloadKeyFromModelName(snapshot.modelName);

      if (payloadType !== deprecatedPayloadTypeLookup && this._hasCustomPayloadKeyFromModelName()) {
        deprecate("You used payloadKeyFromModelName to customize how a type is serialized. Use payloadTypeFromModelName instead.", false, {
          id: 'ds.json-api-serializer.deprecated-payload-type-for-model',
          until: '3.0.0'
        });

        payloadType = deprecatedPayloadTypeLookup;
      }
    } else {
      payloadType = this.payloadKeyFromModelName(snapshot.modelName);
    }