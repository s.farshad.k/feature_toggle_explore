if (isEnabled('ds-links-in-record-array')) {
      this.setProperties({
        content: Ember.A(internalModels),
        isLoaded: true,
        meta: cloneNull(payload.meta),
        links: cloneNull(payload.links)
      });
    } else {
      this.setProperties({
        content: Ember.A(internalModels),
        isLoaded: true,
        meta: cloneNull(payload.meta)
      });
    }