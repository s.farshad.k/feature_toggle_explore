if (isEnabled("ds-payload-type-hooks")) {
        modelName = this.modelNameFromPayloadType(hash.type);
        let deprecatedModelNameLookup = this.modelNameFromPayloadKey(hash.type);

        if (modelName !== deprecatedModelNameLookup && !this._hasCustomModelNameFromPayloadType() && this._hasCustomModelNameFromPayloadKey()) {
          deprecate("You are using modelNameFromPayloadKey to normalize the type for a polymorphic relationship. This is has been deprecated in favor of modelNameFromPayloadType", false, {
            id: 'ds.rest-serializer.deprecated-model-name-for-polymorphic-type',
            until: '3.0.0'
          });

          modelName = deprecatedModelNameLookup;
        }
      } else {
        modelName = this.modelNameFromPayloadKey(hash.type);
      }