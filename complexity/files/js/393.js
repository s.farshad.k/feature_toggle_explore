if (featureFlags.isOn(COMPONENT.FEATURE_FLAGS.FLAG_FABRIC_CONFIGURATION_ASSIGNMENTS) && $scope.hasFabricConfigurations) {
                if ($scope.serviceOrchestrationStatus) {
                    return $scope.serviceOrchestrationStatus.toLowerCase() === 'assigned';
                }
            }