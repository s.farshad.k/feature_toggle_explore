if (this.get('features.proVersion')) {
      const token = this.get('auth').token();
      delimiter = url.indexOf('?') === -1 ? '?' : '&';
      url = '' + url + delimiter + 'token=' + token;
    }