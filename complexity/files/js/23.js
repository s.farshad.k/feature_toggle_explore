if (!this.props.hasFeature('fine_permissions')) {
          return this.props.children({ permission: true });
      }