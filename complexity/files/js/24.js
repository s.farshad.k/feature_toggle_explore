if (this.props.hasFeature('fine_permissions')) {
          if (this.state.isLoading) {
              AppActions.getPermissions(this.props.id, this.props.level);
          }
          this.listenTo(PermissionsStore, 'change', () => {
              this.setState({
                  isLoading: !PermissionsStore.getPermissions(this.props.id, this.props.level),
              });
          });
      }