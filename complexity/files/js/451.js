if (config.featureFlags['pro-version']) {
    delimiter = url.indexOf('?') === -1 ? '?' : '&';
    token = Travis.__container__.lookup('controller:currentUser').get('model.token');
    url = '' + url + delimiter + 'token=' + token;
  }