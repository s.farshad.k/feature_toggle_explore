if (isEnabled('ds-transform-pass-options')) {
          value = transform.serialize(value, attribute.options);
        } else {
          value = transform.serialize(value);
        }