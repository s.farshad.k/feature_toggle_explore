if (proVersion && get(config, 'intercom.enabled')) {
      this.get('intercom').set('user.id', user.id);
      this.get('intercom').set('user.name', user.name);
      this.get('intercom').set('user.email', user.email);
      this.get('intercom').set('user.createdAt', user.first_logged_in_at);
      this.get('intercom').set('user.hash', user.secure_user_hash);
    }