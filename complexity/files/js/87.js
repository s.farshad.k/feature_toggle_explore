const userCanSearch = computed((): boolean => (features.getFlag('search-registration-number') ||
      features.getFlag('search-serial-number')) && userIsAuthed.value)