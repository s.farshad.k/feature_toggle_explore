if (isEnabled('ds-serialize-id')) {
        this.serializeId(snapshot, json, get(this, 'primaryKey'));
      } else {
        var id = snapshot.id;
        if (id) {
          json[get(this, 'primaryKey')] = id;
        }
       }