if (!this.features.isEnabled('organization-settings')) {
        replaceLocation();
        var host = config.aptibleHosts['legacy-dashboard'];
        var url = [host, 'organizations', params.organization_id].join('/');
        replaceLocation(url);
        // Hang this transition
        return new Ember.RSVP.Promise(() => {});
     }