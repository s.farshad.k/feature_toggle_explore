if (isEnabled("ds-payload-type-hooks")) {

        let payloadType = resourceHash[typeProperty];
        let type = this.modelNameFromPayloadType(payloadType);
        let deprecatedTypeLookup = this.modelNameFromPayloadKey(payloadType);

        if (payloadType !== deprecatedTypeLookup && !this._hasCustomModelNameFromPayloadType() && this._hasCustomModelNameFromPayloadKey()) {
          deprecate("You are using modelNameFromPayloadKey to normalize the type for a polymorphic relationship. This has been deprecated in favor of modelNameFromPayloadType", false, {
            id: 'ds.rest-serializer.deprecated-model-name-for-polymorphic-type',
            until: '3.0.0'
          });

          type = deprecatedTypeLookup;
        }

        return {
          id: relationshipHash,
          type: type
        };

      } else {

        let type = this.modelNameFromPayloadKey(resourceHash[typeProperty]);
        return {
          id: relationshipHash,
          type: type
        };

      }