if (featureFlags.isOn('effective-rule-date-plus-one-week') && vm.product.certificationEdition.name === '2014') {
                 return vm.hasAnyRole(['ROLE_ADMIN', 'ROLE_ONC']);
             } else {
                 return vm.hasAnyRole(['ROLE_ADMIN', 'ROLE_ONC', 'ROLE_ACB']);
}