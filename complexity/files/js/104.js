if (isEnabled('ds-transform-pass-options')) {
      if (isEnabled('ds-boolean-transform-allow-null')) {
        if (isNone(deserialized) && options.allowNull === true) {
          return null;
        }
      }
    }