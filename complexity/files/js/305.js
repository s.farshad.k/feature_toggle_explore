if (activity.description.startsWith('Split ') && featureFlags.isOn('better-split')) {
                activity.change.push('Split Developer ' + prev.name + ' into Developers ' + curr[0].name + ' and ' + curr[1].name);
                if (this.interpretedActivity.developers.indexOf(prev.id) === -1) {
                    let that = this;
                    that.interpretedActivity.developers.push(prev.id);
                    that.networkService.getSingleDeveloperActivityMetadata(prev.id, {end: activity.date}).then(response => {
                        let promises = response.map(item => that.networkService.getActivityById(item.id).then(response => that._interpretDeveloper(response)));
                        that.$q.all(promises)
                            .then(response => {
                                that.activity = that.activity
                                    .concat(response)
                                    .filter(a => a.change && a.change.length > 0);
                            });
                    });
                }
            }