if (config.featureFlags['pro-version']) {
    let token = Travis.__container__.lookup('controller:currentUser').get('model.token');
    return `${prefix}/${slug}.svg?token=${token}${branch ? '&branch=' + branch : ''}`;
  }