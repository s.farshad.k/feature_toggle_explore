if (Ember.FEATURES.isEnabled('ds-new-serializer-api') && this.get('isNewSerializerAPI')) {
      _newNormalize.apply(this, arguments);
      return this._super(...arguments);
    }