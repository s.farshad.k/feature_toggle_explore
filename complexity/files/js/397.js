return {
   lcpCloudRegionId: lcpRegion,
   cloudOwner: featureFlags.isOn(COMPONENT.FEATURE_FLAGS.FLAG_1810_CR_ADD_CLOUD_OWNER_TO_MSO_REQUEST) ? cloudOwner : undefined,
   tenantId: getValueFromList(FIELD.ID.TENANT, parameterList)
  };