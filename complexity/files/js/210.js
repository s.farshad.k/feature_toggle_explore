if (featureFlags.isOn('listing-edit')) {
            listingStates['listing-edit-on'].forEach(state => {
                $uiRouter.stateRegistry.register(state);
            });
        } else {
            listingStates['listing-edit-off'].forEach(state => {
                $uiRouter.stateRegistry.register(state);
            });
        }