if (this.get('features.proVersion')) {
      if (Travis.pusher && Travis.pusher.ajaxService) {
        return Travis.pusher.ajaxService.post(Pusher.channel_auth_endpoint, {
          socket_id: Travis.pusher.pusherSocketId,
          channels: ['private-job-' + this.get('id')]
        }).then(() => {
          return Travis.pusher.subscribe(this.get('channelName'));
        });
      }
    }