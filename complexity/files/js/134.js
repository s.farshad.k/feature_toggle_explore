if (isEnabled("ds-payload-type-hooks")) {

  JSONSerializer.reopen({

    /**
      @method modelNameFromPayloadType
      @public
      @param {String} type
      @return {String} the model's modelName
      */
    modelNameFromPayloadType(type) {
      return normalizeModelName(type);
    },

    _hasCustomModelNameFromPayloadKey() {
      return this.modelNameFromPayloadKey !== JSONSerializer.prototype.modelNameFromPayloadKey;
    }

  });

}