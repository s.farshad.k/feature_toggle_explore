if (isEnabled('ds-improved-ajax')) {
      const request = this._requestFor({
        store, type, ids, snapshots,
        requestType: 'findMany'
      });

      return this._makeRequest(request);
    } else {
      var url = this.buildURL(type.modelName, ids, snapshots, 'findMany');
      return this.ajax(url, 'GET', { data: { ids: ids } });
    }