configCat.getValue('androidVersion', 'Default', storeVersion => {
        const currentVersion = DeviceInfo.getVersion();
        if (currentVersion < storeVersion) {
          showMessage({
            type: 'info',
            message: 'Novo update disponível!',
            description: `Você está rodando a versão ${currentVersion}, porém a versão ${storeVersion} já está disponível na Google Play.`,
            duration: 5000,
          });
        }
      });