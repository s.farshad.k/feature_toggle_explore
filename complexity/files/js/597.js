if (!this.get('features.proVersion') || this.get('features.enterpriseVersion')) {
       transition.abort();
      model.isUser ? this.transitionTo('account.repositories') : this.transitionTo('organization.repositories', model);
     }