{hasFeature('demo_feature') && (
                                    <a
                                      style={{ color: 'white' }}
                                      className="link--footer active"
                                      href="https://docs.bullet-train.io"
                                    >
                                        <i className="icon mr-2 ion-ios-star"/>
                                        Super cool demo feature!
                                    </a>
                                )}