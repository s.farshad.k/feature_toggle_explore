if (isEnabled('ds-improved-ajax')) {
      const request = this._requestFor({
        store, type, snapshot,
        requestType: 'deleteRecord'
      });

      return this._makeRequest(request);
    } else {
      var id = snapshot.id;