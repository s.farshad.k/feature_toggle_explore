if (isEnabled('ds-improved-ajax')) {
      const request = this._requestFor({
        store, type, snapshot,
        requestType: 'createRecord'
      });

      return this._makeRequest(request);
    } else {
      var data = {};
      var serializer = store.serializerFor(type.modelName);
      var url = this.buildURL(type.modelName, null, snapshot, 'createRecord');

      serializer.serializeIntoHash(data, type, snapshot, { includeId: true });

      return this.ajax(url, "POST", { data: data });
    }