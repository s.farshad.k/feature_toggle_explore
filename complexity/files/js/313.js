if (featureFlags.isOn('surveillance-reporting')) {
                    surveillanceStates['surveillance-reports-on'].forEach(state => {
                        $uiRouter.stateRegistry.deregister(state.name);
                        $uiRouter.stateRegistry.register(state);
                        needsReload = needsReload || $state.$current.name === state.name;
                    });
                }