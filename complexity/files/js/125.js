if (isEnabled('ds-improved-ajax')) {
      return this._super(...arguments);
    } else {
      var url = this.buildURL(type.modelName, ids, snapshots, 'findMany');
      return this.ajax(url, 'GET', { data: { filter: { id: ids.join(',') } } });
    }