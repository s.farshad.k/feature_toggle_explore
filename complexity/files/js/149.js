if (isEnabled('ds-reset-attribute')) {
  Model.reopen({
    resetAttribute(attributeName) {
      if (attributeName in this._internalModel._attributes) {
        this.set(attributeName, this._internalModel.lastAcknowledgedValue(attributeName));
      }
    }
  });
}