if (isEnabled('ds-extended-errors')) {
  DS.UnauthorizedError = UnauthorizedError;
  DS.ForbiddenError    = ForbiddenError;
  DS.NotFoundError     = NotFoundError;
  DS.ConflictError     = ConflictError;
}