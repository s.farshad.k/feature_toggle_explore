if (proVersion) {
      channels = channels.map(function (channel) {
        if (channel.match(/^private-/)) {
          return channel;
        } else {
          return 'private-' + channel;
        }
      });
    }