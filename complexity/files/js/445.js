if (this.features.get('debugLogging')) {
      const { status = 'UNKNOWN' } = response;
      // eslint-disable-next-line
      console.log(`[ERROR] Fetch error (${status}): ${response}`);
     }