this.state = this.props.hasFeature('fine_permissions') ? {
          isLoading: !PermissionsStore.getPermissions(this.props.id, this.props.level),
      } : {};