if (isEnabled("ds-payload-type-hooks")) {

  RESTSerializer.reopen({

    /**
      `modelNameFromPayloadType` can be used to change the mapping for a DS model
      name, taken from the value in the payload.

      Say your API namespaces the type of a model and returns the following
      payload for the `post` model, which has a polymorphic `user` relationship:

      ```javascript
      // GET /api/posts/1
      {
        "post": {
          "id": 1,
          "user": 1,
          "userType: "api::v1::administrator"
        }
      }
      ```

      By overwriting `modelNameFromPayloadType` you can specify that the
      `administrator` model should be used:

      ```app/serializers/application.js
      import RESTSerializer from "ember-data/serializers/rest";

      export default RESTSerializer.extend({
        modelNameFromPayloadType(payloadType) {
          return payloadType.replace('api::v1::', '');
        }
      });
      ```

      By default the modelName for a model is its name in dasherized form.
      Usually, Ember Data can use the correct inflection to do this for you. Most
      of the time, you won't need to override `modelNameFromPayloadType` for this
      purpose.

      Also take a look at
      [payloadTypeFromModelName](#method_payloadTypeFromModelName) to customize
      how the type of a record should be serialized.

      @method modelNameFromPayloadType
      @public
      @param {String} payloadType type from payload
      @return {String} modelName
    */
    modelNameFromPayloadType(payloadType) {
      return singularize(normalizeModelName(payloadType));
    },

    /**
      `payloadTypeFromModelName` can be used to change the mapping for the type in
      the payload, taken from the model name.

      Say your API namespaces the type of a model and expects the following
      payload when you update the `post` model, which has a polymorphic `user`
      relationship:

      ```javascript
      // POST /api/posts/1
      {
        "post": {
          "id": 1,
          "user": 1,
          "userType": "api::v1::administrator"
        }
      }
      ```

      By overwriting `payloadTypeFromModelName` you can specify that the
      namespaces model name for the `administrator` should be used:

      ```app/serializers/application.js
      import RESTSerializer from "ember-data/serializers/rest";

      export default RESTSerializer.extend({
        payloadTypeFromModelName(modelName) {
          return "api::v1::"  modelName;
        }
      });
      ```

      By default the payload type is the camelized model name. Usually, Ember
      Data can use the correct inflection to do this for you. Most of the time,
      you won't need to override `payloadTypeFromModelName` for this purpose.

      Also take a look at
      [modelNameFromPayloadType](#method_modelNameFromPayloadType) to customize
      how the model name from should be mapped from the payload.

      @method payloadTypeFromModelName
      @public
      @param {String} modelname modelName from the record
      @return {String} payloadType
    */
    payloadTypeFromModelName(modelName) {
      return camelize(modelName);
    },

    _hasCustomModelNameFromPayloadKey() {
      return this.modelNameFromPayloadKey !== RESTSerializer.prototype.modelNameFromPayloadKey;
    },

    _hasCustomModelNameFromPayloadType() {
      return this.modelNameFromPayloadType !== RESTSerializer.prototype.modelNameFromPayloadType;
    },

    _hasCustomPayloadTypeFromModelName() {
      return this.payloadTypeFromModelName !== RESTSerializer.prototype.payloadTypeFromModelName;
    },

    _hasCustomPayloadKeyFromModelName() {
      return this.payloadKeyFromModelName !== RESTSerializer.prototype.payloadKeyFromModelName;
    },

  });

}