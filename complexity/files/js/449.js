if (ENV.featureFlags['pro-version'] && !process.env.TRAVIS_ENTERPRISE) {
      ENV.apiEndpoint = 'https://api.travis-ci.com';
      ENV.pusher.key = '59236bc0716a551eab40';
      ENV.pusher.channelPrefix = 'private-';
      ENV.pagesEndpoint = 'https://billing.travis-ci.com';
      ENV.billingEndpoint = 'https://billing.travis-ci.com';
      ENV.statusPageStatusUrl = statusPageStatusUrl;
      ENV.sentry = {
        dsn: sentryDSN
      };
      ENV.endpoints = {
        sshKey: true,
        caches: true
      };
      ENV.userlike = true;
      ENV.beacon = true;
      ENV.urls = {
        legal: ENV.billingEndpoint + "/pages/legal",
        imprint: ENV.billingEndpoint + "/pages/imprint",
        security: ENV.billingEndpoint + "/pages/security",
        terms: ENV.billingEndpoint + "/pages/terms"
      };
      ENV.heap = {
        projectId: '1556722898'
      }
    }