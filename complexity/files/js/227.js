if (featureFlags.isOn('effective-rule-date')) {
                        collectionsStates['effective-rule-date'].forEach(state => {
                            if ($uiRouter.stateRegistry.get(state.name)) {
                                $uiRouter.stateRegistry.deregister(state.name);
                            }
                            $uiRouter.stateRegistry.register(state);
                            needsReload = needsReload || $state.$current.name === state.name;
                        });
                    }