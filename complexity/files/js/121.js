if (isEnabled('ds-improved-ajax')) {
      const request = this._requestFor({
        store, type, sinceToken, query,
        snapshots: snapshotRecordArray,
        requestType: 'findAll'
      });

      return this._makeRequest(request);
    } else {
      const url = this.buildURL(type.modelName, null, null, 'findAll');

      if (sinceToken) {
        query.since = sinceToken;
      }

      return this.ajax(url, 'GET', { data: query });
    }