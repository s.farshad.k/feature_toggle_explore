{this.props.hasFeature('webhooks') && (
                                                <FormGroup className="m-y-3">
                                                    <Row className="mb-3" space>
                                                        <h3 className="m-b-0">Webhooks</h3>
                                                        <Button onClick={this.createWebhook}>
                                                        Create webhook
                                                        </Button>
                                                    </Row>
                                                    {webhooksLoading && !webhooks ? (
                                                        <Loader/>
                                                    ) : (
                                                        <PanelSearch
                                                          id="webhook-list"
                                                          title={(
                                                              <Tooltip
                                                                title={<h6 className="mb-0">Webhooks <span className="icon ion-ios-information-circle"/></h6>}
                                                                place="right"
                                                              >
                                                                  {Constants.strings.WEBHOOKS_DESCRIPTION}
                                                              </Tooltip>
                                                    )}
                                                          className="no-pad"
                                                          icon="ion-md-cloud"
                                                          items={webhooks}
                                                          renderRow={webhook => (
                                                              <Row
                                                                onClick={() => {
                                                                    this.editWebhook(webhook);
                                                                }} space className="list-item clickable cursor-pointer"
                                                                key={webhook.id}
                                                              >
                                                                  <Flex>
                                                                      <a href="#">
                                                                          {webhook.url}
                                                                      </a>
                                                                      <div className="list-item-footer faint">
                                                                          Created
                                                                          {' '}
                                                                          {moment(webhook.created_date).format('DD/MMM/YYYY')}
                                                                      </div>
                                                                  </Flex>
                                                                  <Row>
                                                                      <Switch checked={webhook.enabled}/>
                                                                      <button
                                                                        id="delete-invite"
                                                                        type="button"
                                                                        onClick={(e) => {
                                                                            e.stopPropagation();
                                                                            e.preventDefault();
                                                                            this.deleteWebhook(webhook);
                                                                        }}
                                                                        className="btn btn--with-icon ml-auto btn--remove"
                                                                      >
                                                                          <RemoveIcon/>
                                                                      </button>
                                                                  </Row>
                                                              </Row>
                                                          )}
                                                          renderNoResults={(
                                                              <Panel
                                                                id="users-list"
                                                                icon="ion-md-cloud"
                                                                title={(
                                                                    <Tooltip
                                                                      title={<h6 className="mb-0">Webhooks <span className="icon ion-ios-information-circle"/></h6>}
                                                                      place="right"
                                                                    >
                                                                        {Constants.strings.WEBHOOKS_DESCRIPTION}
                                                                    </Tooltip>
                                                        )}
                                                              >
                                                          You currently have no webhooks configured for this environment.
                                                              </Panel>
                                                    )}
                                                          isLoading={this.props.webhookLoading}
                                                        />
                                                    )}
                                                </FormGroup>
                                            )}