{hasFeature('edit_account') && (
                                <NavLink
                                  id="organisation-settings-link"
                                  activeClassName="active"
                                  className="link--footer"
                                  to={`/project/${this.props.projectId}/environment/${this.props.environmentId}/account-settings`}
                                >
                                  Account Settings
                                </NavLink>
                                 )}