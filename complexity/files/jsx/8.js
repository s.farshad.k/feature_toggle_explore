return  hasFeature("try_it") ? (
            <Panel
                icon={"ion-md-code"}
                title={"Try it out"}
            >
                <div>
                    <div className={"text-center"}>
                        <p className={"faint-lg"}>
                            {this.props.title}
                        </p>
                        <div>
                            <Button id="try-it-btn" disabled={this.state.isLoading} onClick={this.request}>
                                {this.state.data ? "Test again" : "Run test"}
                            </Button>
                        </div>
                    </div>
                    {this.state.data && (
                        <div id="try-it-results">
                            <FormGroup/>
                            <Highlight className={"json"}>
                                {this.state.data}
                            </Highlight>
                        </div>
                    )}
                    {this.state.isLoading && !this.state.data && <div className={"text-center"}>
                        <Loader/>
                    </div>}
                </div>
            </Panel>
        ): <div/>;