{hasFeature('identity_segments') && (
                                            <IdentitySegmentsProvider>
                                                {({ isLoading: segmentsLoading, segments }) => (segmentsLoading ? <div className="text-center"><Loader/></div> : (
                                                    <FormGroup>
                                                        <PanelSearch
                                                          id="user-segments-list"
                                                          className="no-pad"
                                                          icon="ion-ios-globe"
                                                          title="Segments"
                                                          items={segments ? segments.results : []}
                                                          acti
                                                          renderRow={({ name, id, enabled, created_date, type }, i) => (
                                                              <Row
                                                                className="list-item"
                                                                space
                                                                key={i}
                                                              >
                                                                  <div
                                                                    className="flex flex-1"
                                                                  >
                                                                      <Row>
                                                                          <span data-test={`segment-${i}-name`} className="bold-link">
                                                                              {name}
                                                                          </span>
                                                                      </Row>
                                                                      <div className="list-item-footer faint">
                                                                            Created
                                                                          {' '}
                                                                          {moment(created_date).format('DD/MMM/YYYY')}
                                                                      </div>
                                                                  </div>
                                                              </Row>
                                                          )
                                                            }
                                                          renderNoResults={(
                                                              <Panel
                                                                icon="ion-ios-globe"
                                                                title="Segments"
                                                                className="text-center"
                                                              >
                                                                    This user is not part of any segment.
                                                              </Panel>
                                                            )}
                                                          filterRow={({ name }, search) => name.toLowerCase().indexOf(search) > -1}
                                                        />
                                                    </FormGroup>
                                                ))}
                                            </IdentitySegmentsProvider>
                                        )}