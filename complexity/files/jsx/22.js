{hasFeature('fine_permissions') && (
                                                    <div>
                                                        <Row space className="mt-5">
                                                            <h5>User groups</h5>
                                                            <Button
                                                              className="mr-2"
                                                              id="btn-invite" onClick={() => openModal('Create Group',
                                                                  <CreateGroupModal orgId={organisation.id}/>)}
                                                              type="button"
                                                            >
                                                                  Create Group
                                                            </Button>
                                                        </Row>
                                                        <p>Groups allow you to manage permissions for viewing and editing projects, features and environments.</p>
                                                        <UserGroupList orgId={organisation.id}/>
                                                    </div>
                                                    )}