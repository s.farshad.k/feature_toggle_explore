<ToggleFeature flag={'distance'}>
                <Box pad={{'bottom': '10px'}}>
                  <Heading margin='none' level="5">distance {distance} miles</Heading>
                  <RangeInput
                    value={distance}
                    onChange={event => setOption('distance', event.target.value)}
                  />
                </Box>
              </ToggleFeature>