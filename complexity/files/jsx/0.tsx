<ToggleFeature key="edit-button-key" flag="showEditButton">
            <EditButton
                key="edit-button"
                mode={props.editMode}
                onClick={() => {
                    props.setEditMode(props.editMode === EditMode.View ? EditMode.Edit : EditMode.View);
                }}
            />
        </ToggleFeature>