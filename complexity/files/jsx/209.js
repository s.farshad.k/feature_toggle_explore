<FeatureFlag name="send-analytics-to-pipeline">
        {yes =>
          yes ? (
            <FabricAnalyticsListeners client={mockAnalyticsClient}>
              <div>{children}</div>
            </FabricAnalyticsListeners>
          ) : (
            children
          )
        }
      </FeatureFlag>