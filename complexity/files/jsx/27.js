{hasFeature('audit_log') && (

                                            <li className="env-nav__item flex-row">
                                                <Link
                                                  id="audit-log-link"
                                                  activeClassName="active"
                                                  to={`/project/${project.id}/environment/${environment.api_key}/audit-log`
                                                    }
                                                >
                                                    Audit Log
                                                </Link>
                                            </li>
                                            )}