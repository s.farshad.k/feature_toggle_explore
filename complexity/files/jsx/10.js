{hasFeature('segments') && (
                                                <li className="env-nav__item flex-row">
                                                    <Link
                                                        activeClassName={"active"}
                                                        to={`/project/${project.id}/environment/${environment.api_key}/segments`
                                                        }>Segments</Link>
                                                </li>
                                            )}