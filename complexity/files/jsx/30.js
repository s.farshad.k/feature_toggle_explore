{!hasFeature('free_tier') ? (
                                    <div className={"col-md-3 pricing-panel"}>
                                        <div className="panel panel-default">
                                            <div className="panel-content">
                                                <p className="featured"> </p>
                                                <p className="pricing-price">Side Project</p>
                                                <img src="/images/growth.svg" alt="free icon" className="pricing-icon"/>
                                                <p className="pricing-type">$5</p>
                                                <p className="text-small text-center">Billed monthly</p>
                                                <Link to={`/${redirect}#sign-up`} className="pricing-cta blue">Start free trial</Link>
                                            </div>
                                            <div className="panel-footer">
                                                <p className="text-small text-center link-style">What's included</p>
                                                <ul className="pricing-features">
                                                    <li><p>Up to 2,000 Monthly Active Users</p></li>
                                                    <li><p>Unlimited Administrator Accounts</p></li>
                                                    <li><p>Unlimited Projects</p></li>
                                                    <li><p>Unlimited Environments</p></li>
                                                    <li><p>Unlimited Feature Flags</p></li>
                                                    <li><p>Email Technical Support</p></li>
                                                </ul>
                                            </div>
                                         </div>
                                    </div>
                                ) : (
                                    <div className={"col-md-3 pricing-panel"}>
                                        <div className="panel panel-default">
                                            <div className="panel-content">
                                                <p className="featured"> </p>
                                                <p className="pricing-price">Free</p>
                                                <img src="/images/growth.svg" alt="free icon" className="pricing-icon"/>
                                                <p className="pricing-type">Free</p>
                                                <p className="text-small text-center">more flags than the UN</p>
                                                <a href="register" className="pricing-cta blue">Sign up</a>
                                            </div>
                                            <div className="panel-footer">
                                                <p className="text-small text-center link-style">What's included</p>
                                                <ul className="pricing-features">
                                                    <li><p>Up to 2,000 Monthly Active Users</p></li>
                                                    <li><p>Unlimited Administrator Accounts</p></li>
                                                    <li><p>Unlimited Projects</p></li>
                                                    <li><p>Unlimited Environments</p></li>
                                                    <li><p>Unlimited Feature Flags</p></li>
                                                </ul>
                                            </div>
                                         </div>
                                     </div>
                                )}