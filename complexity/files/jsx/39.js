{hasFeature('forgot_password') && (
                                                            <div className={"text-right"}>
                                                                <Link to={`/password-recovery${redirect}`}
                                                                      onClick={this.showForgotPassword}>Forgot
                                                                    password?</Link>
                                                            </div>
                                                        )}