{this.props.segments && hasFeature("segment_overrides") && (
                                    <FormGroup className={"mb-4"}>
                                        <Tooltip
                                            title={<label className="cols-sm-2 control-label">Segment Overrides (Optional)</label>}
                                            place="right"
                                        >
                                            {Constants.strings.SEGMENT_OVERRIDES_DESCRIPTION}
                                        </Tooltip>
                                        <SegmentOverrides
                                            type={type}
                                            value={this.props.segmentOverrides}
                                            segments={this.props.segments}
                                            onChange={this.props.updateSegments}/>
                                    </FormGroup>
                                )}