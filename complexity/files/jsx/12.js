{this.props.hasFeature('segments') && (
                                                        <NavLink
                                                          id="segments-link"
                                                          className="project-nav__item project-list__item  project-settings-link"
                                                          activeClassName="active"
                                                          to={`/project/${project.id}/environment/${this.props.environmentId}/segments`
                                                              }
                                                        >
                                                                Segments
                                                        </NavLink>
                                                    )}