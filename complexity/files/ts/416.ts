if (this.featureFlagsService.getFlagState(Features.FLAG_FLASH_REPLACE_VF_MODULE)) {
        return from(this.retrieveLatestVersionAndSetServiceInstance(serviceInstance.modelInfo.modelInvariantId).then((response) => {
          this.setServiceLatestAvailableVersion(serviceInstance, response);
          this.applyRequestsResponsesToStateAndInitServiceInstance(serviceModelId, serviceInstance, serviceModel);
          return true;
        }));
      }