if (featureFlags.getFlag('bcrs-create-ui-enabled')) {
      new Vue({
        vuetify,
        router,
        store,
        render: h => h(App)
      }).$mount('#app')
    }