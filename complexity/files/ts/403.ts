if (FeatureFlagsService.getFlagState(Features.FLAG_2002_VFM_UPGRADE_ADDITIONAL_OPTIONS, this._store)) {
      this._iframeService.addClassOpenModal('content');
      this._dialogService.addDialog(GenericFormPopupComponent, {
        type: PopupType.VF_MODULE_UPGRADE,
        uuidData: <any>{
          serviceId: serviceModelId,
          modelName: node.data.modelName,
          vFModuleStoreKey: node.data.dynamicModelName,
          vnfStoreKey: node.parent.data.vnfStoreKey,
          modelId: node.data.modelId,
          type: node.data.type,
          popupService: this._vfModuleUpgradePopupService,
        },
        node: node,
        isUpdateMode: false
      });
    }