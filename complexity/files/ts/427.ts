if( !this._featureFlagsService.getFlagState(Features.FLAG_2006_VFMODULE_TAKES_TENANT_AND_REGION_FROM_VNF)) {
      result.push(this._sharedControllersService.getLcpRegionControl(serviceId, vfModuleInstance, result));
      result.push(this._sharedControllersService.getLegacyRegion(vfModuleInstance));
      result.push(this._sharedControllersService.getTenantControl(serviceId, vfModuleInstance));
    }