if (FeatureFlagsService.getFlagState(Features.FLAG_1902_VNF_GROUPING, this._store)) {
      this._aaiService.getServiceModelById(item['serviceModelId']).subscribe((result)=>{
        const serviceModel =  new ServiceModel(result);

        if (this.isDrawingBoardViewEdit(serviceModel)) {
          this.navigateToNewViewEdit(item, DrawingBoardModes.EDIT);
          return;
        }

        this.navigateToNewViewOnlyOrOldEditView(item);

      });
    }