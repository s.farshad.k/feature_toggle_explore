return FeatureFlagsService.getFlagState(Features.FLAG_1908_RESUME_MACRO_SERVICE, this.store) &&
        serviceInstance.vidNotions.instantiationType.toLowerCase() === "macro" &&
        serviceInstance.subscriptionServiceType.toLowerCase() !== "transport" &&
        serviceInstance.orchStatus &&
        (serviceInstance.orchStatus.toLowerCase() === "assigned" ||
          serviceInstance.orchStatus.toLowerCase() === "inventoried");