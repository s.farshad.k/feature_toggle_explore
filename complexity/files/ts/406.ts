!!this.store.getState().global.flags['FLAG_2006_VFM_SDNC_PRELOAD_FILES'] ? {
        type: 'UPLOAD_FILE',
        dataTestId: 'sdnc_pereload_upload_link',
        uploadMethod: (form: FormGroup) : Promise<boolean> => {
          // this -> files item
          return this._aaiService.sdncPreload().toPromise()
            .then((response : boolean)=>{
              return response;
            }).catch(err => {
              return false;
            });
        },
        isDisabled: (form: FormGroup): boolean => {
          return !form.controls[SDN_C_PRE_LOAD].value;
        },
        onSuccess: (form: FormGroup): void => {
          MessageModal.showMessageModal({
            text: 'The pre-load file(s) have been uploaded successfully.',
            type: "success",
            title: 'Success',
            buttons: [{type: ButtonType.success, size: 'large', text: 'OK', closeModal: true}]
          })
        },
        onFailed: (form: FormGroup) : void=> {
          MessageModal.showMessageModal({
            text: 'Failed to upload one or more of the files, please retry.',
            type: "error",
            title: 'Failure',
            buttons: [{type: ButtonType.error, size: 'large', text: 'OK', closeModal: true}]
          })
         }
      } : null