if (FeatureFlagsService.getFlagState(Features.FLAG_FLASH_REPLACE_VF_MODULE, this._store) &&
      this.isThereAnUpdatedLatestVersion(serviceModelId)) {
      return this.shouldShowButtonGeneric(node, VNFMethods.UPGRADE);
    }