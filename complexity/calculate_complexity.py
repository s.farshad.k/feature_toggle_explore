import subprocess
from os import listdir
from os.path import isfile, join
import os
import time

path="./complexity/files/js/"
report="./complexity/report.txt"
files = [f for f in listdir(path) if isfile(join(path, f))]
ft=True
for file in files:
    os.system('cr -e {}{} -o {} '.format(path,file,report))
    # print(str(response).strip().find('error'))
    time.sleep(0.2)
    fileLink = open('{}'.format(report),mode='r') 
    all_of_it = fileLink.read()
    fileLink.close()
    try:
        if(all_of_it.find(file)!=-1 and not(ft)):
            raise file
    except Exception as e:
        print(file)
    ft=False

