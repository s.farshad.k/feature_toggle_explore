import pickle
import numpy as np
import requests
import sys

manual_deletes=[
    'tiagodeluna/devops-lab',
    'solve-hq/LaunchDarkly-relay-fargate',
    'sohankunkerkar/DevOpsMileStone-2',
    'leuthera/playground',
    'AppImage/appimage.github.io',
    'libreappimagehub101/libreappimagehub101.github.io',
    'tdeekens/flopflip',
    'SofiMat/tarsiers-shoppy',
    'trmchale1/android-backend',
    'Poonamgirdhar/MainProject',
    'mshoote/BTJSC',
    'rgosdin/iCloud-Drive',
    'berickson1/Playground',
    'athmakuru/chef-automate',
    'chef/automate',
    'sevenwestmedia-labs/etrigan',
    'meetup/meetup-web-platform',
    'gorillaking/LaunchDarkly',
    'bcgov/sbc-common-components',
    'OnyxPrime/FeatureFlagsDemo',
    'brommz/feature-toggle-poc',
    'ALM-Rangers/vsts-extension-featureflag-sample',
    'mdeggies/techbooks-launchdarkly',
    'BulletTrainHQ/bullet-train-examples',
    'DawsonGraham/launch_darkly_exercise',
    'textlint/textlint'
]
# #--------------------------------------------------------------------------------
# #check validity of repos
# f = open('records.pckl', 'rb')
# records=pickle.load(f);

# f = open('init_repos.pckl', 'rb')
# repos=pickle.load(f);
# remained_repos=np.unique(repos)
# print(len(remained_repos));

# record_by_repo = {}
# for key in records.keys():
#     for record in records[key]:
#         record_by_repo[record['repo']] = record

# paths ={}
# invalid_pkg=[]
# cnt=0
# howmany=0
# for repos in record_by_repo.keys():
#     cnt+=1
#     print (str(cnt)+'/'+str(len(record_by_repo.keys())), end="\r")

#     # print(record_by_repo[repos])
#     # exit()
#     # print(record_by_repo[repos]['path'])
#     url = 'https://raw.githubusercontent.com/{}/master/{}'.format(record_by_repo[repos]['repo'],record_by_repo[repos]['path'])
#     # print(url)
#     params = dict(
#         origin='Chicago,IL',
#         destination='Los+Angeles,CA',
#         waypoints='Joplin,MO|Oklahoma+City,OK',
#         sensor='false'
#     )
#     try:
#         resp = requests.get(url=url, params=params)
#         data = resp.json() 
#         # print(repos)
#         # print(record_by_repo[repos]['lib'])
#         at_lib='@'+record_by_repo[repos]['lib']
#         if (not('dependencies' in data.keys() and 
#                     (record_by_repo[repos]['lib'] in data['dependencies'].keys() or at_lib in data['dependencies'].keys()) )
#             and not('devDependencies' in data.keys() and 
#                     (record_by_repo[repos]['lib'] in data['devDependencies'].keys() or at_lib in data['devDependencies'].keys() )    )
#             ):
#             # print(repos+'('+record_by_repo[repos]['lib']+','+at_lib+') -> '+record_by_repo[repos]['path'])
#             invalid_pkg.append(repos)
#             # print()
#         # elif( '/' in record_by_repo[repos]['path']):
#         #     howmany+=1
#             # print(repos+'('+record_by_repo[repos]['lib']+') -> '+record_by_repo[repos]['path'])
#             # print()
#     except Exception as e:
#         invalid_pkg.append(repos)
# # print(howmany)
# f = open('./invalid_pkgs.pckl', 'wb')
# pickle.dump(invalid_pkg, f)
# f.close()
# print()
# print(len(invalid_pkg))
# exit()

# # # -----------------------------------------------------------------
# here we find any remaining ones
# f = open('init_repos.pckl', 'rb')
# initial_repos=np.unique(pickle.load(f));

# f = open('records.pckl', 'rb')
# records=pickle.load(f);
# record_by_repo = {}
# for key in records.keys():
#     for record in records[key]:
#         record_by_repo[record['repo']] = record

# f = open('invalid_pkgs.pckl', 'rb')
# invalid_repos=pickle.load(f);

# f = open('statistics_all_before.pckl', 'rb')
# first_records=pickle.load(f);
# f = open('statistics.pckl', 'rb')
# second_records=pickle.load(f);


# all_records = {**first_records, **second_records} 

# list_repos=np.array(list(all_records.keys()))

# diff_repos= []
# for repo in initial_repos:
#     if not(repo in list_repos):
#         diff_repos.append(repo)

# remained_list = []
# remained_repos = []
# for diff_repo in diff_repos:
#     if not(diff_repo in invalid_repos):
#         remained_list.append(record_by_repo[diff_repo])
#         remained_repos.append(diff_repo)
# print(remained_list)
# f = open('./final_remained_repos.pckl', 'wb')
# pickle.dump(remained_repos, f)
# f.close()

#----------------------------------------------------------------------
#delete repos that needs to be removed by manual observation


# converge all the result under one variable and delete the invalid repositories
f = open('statistics_all_before.pckl', 'rb')
first_records=pickle.load(f);
f = open('statistics.pckl', 'rb')
second_records=pickle.load(f);
f = open('r1_statistics.pckl', 'rb')
third_records=pickle.load(f);
f = open('r2_statistics.pckl', 'rb')
fourth_records=pickle.load(f);

f = open('invalid_pkgs.pckl', 'rb')
invalid_repos=pickle.load(f);



all_records = {**first_records, **second_records, **third_records, **fourth_records} 
print(len(all_records))

list_repos=np.array(list(all_records.keys()))
print(len(list_repos))
# f = open('./final_remained_records.pckl', 'wb')
# pickle.dump(remained_list, f)
# f.close()

for repo in list_repos:
    if repo in invalid_repos:
        del all_records[repo]
    elif repo in manual_deletes:
        del all_records[repo]

#now all_records are pure! 
# let's save it
f = open('./final_retrieved_data.pckl', 'wb')
pickle.dump(all_records, f)
f.close()

#-------------------------------------------------------------------
# lets go for fork detection tes


