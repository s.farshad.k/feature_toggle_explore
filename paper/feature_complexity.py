from datetime import date
import matplotlib.ticker as ticker
import matplotlib.pyplot as plt
import pandas as pd
import pickle
import numpy as np
import time
from matplotlib import rcParams
import seaborn as sns
#----------------------------+++++++++
from sklearn import svm
from sklearn.model_selection import RepeatedKFold
from sklearn import metrics
from sklearn.preprocessing import StandardScaler
from sklearn.neural_network import MLPClassifier
import math

sns.set()

f = open('./paper/analyze_chunks_final_v5.pckl', 'rb')
records = pickle.load(f)

f = open('./feature_status_and_duration_v2.pckl', 'rb')
feature_stat_and_duration = pickle.load(f)

results = {}
dates = []
# feature_date_per_repository = {}
repos = []
# first get all the change dates
# df=pd.DataFrame(columns=['LOC','Cyclomatic complexity number','HE','MI','FOD','lib','repo','lang','duration'])
df1 = pd.DataFrame(columns=['LOC', 'lib', 'repo', 'duration', 'Removal Status'])
df2 = pd.DataFrame(columns=['Cyclomatic complexity number', 'lib', 'repo', 'duration', 'Removal Status'])
df3 = pd.DataFrame(
    columns=['Halstead effort', 'lib', 'repo', 'duration', 'Removal Status'])
df4 = pd.DataFrame(columns=['Maintainability index',
                            'lib', 'repo', 'duration', 'Removal Status'])
df5 = pd.DataFrame(columns=['First-order density',
                            'lib', 'repo', 'duration', 'Removal Status'])
df6 = pd.DataFrame(columns=['LLOC',
                            'lib', 'repo', 'duration', 'Removal Status'])

duration_vs_LOC = {}
duration_vs_CCN = {}
duration_vs_HE = {}
duration_vs_MI = {}
duration_vs_FOD = {}
duration_vs_LLOC = {}
feature_info = {}
# ----------------------------------------------------------
fig, axlist = plt.subplots(3, 2)  # , gridspec_kw={'width_ratios': [[1, 3]]})
ax = ax = axlist.flatten()
ax[5].axis('off')
# for ax in axlist.flatten():
#     line1, = ax.plot(np.random.random(100), label='data1')
#     line2, = ax.plot(np.random.random(100), label='data2')
#     line3, = ax.plot(np.random.random(100), 'o', label='data3')

# fig.subplots_adjust(top=0.9, left=0.1, right=0.9, bottom=0.12)  # create some space below the plots by increasing the bottom-value
# # it would of course be better with a nicer handle to the middle-bottom axis object, but since I know it is the second last one in my 3 x 3 grid...
# plt.show()

# exit();

# ----------------------------------------------------------
# plot duration vs LOC
for record in records:
    if not(record['feature_id'] in feature_info.keys()):
        feature_info[record['feature_id']] = {'lib': record['lib'], 'repo': record['repository'],
                                              'duration': feature_stat_and_duration[record['feature_id']]['duration'], 'Removal Status': feature_stat_and_duration[record['feature_id']]['removed']}

    if not(record['feature_id'] in duration_vs_LOC.keys()):
        duration_vs_LOC[record['feature_id']] = record['LOC']+1
    else:
        duration_vs_LOC[record['feature_id']] += record['LOC']+1
for feature_id in feature_info:
    if not(feature_info[feature_id]['Removal Status']):
        feature_info[feature_id]['duration']-=14;
    df1 = df1.append(pd.DataFrame([[feature_id, duration_vs_LOC[feature_id], feature_info[feature_id]['lib'], feature_info[feature_id]['repo'],
                                    feature_info[feature_id]['duration'], feature_info[feature_id]['Removal Status']]], columns=['id', 'LOC', 'lib', 'repo', 'duration', 'Removal Status']), ignore_index=True)
print(len(feature_info))
# exit()
g = sns.scatterplot(x="LOC", y="duration",
                hue="lib", style='Removal Status', data=df1,ax=ax[0])
ax[0].set_ylabel("Duration (Days)",fontsize=15)
ax[0].legend_.remove()
g = g.set(xlim=(0,200),ylim=(0,1800))
# ------------------------------------------
# plot duration vs LOC
for record in records:
    if not(record['feature_id'] in feature_info.keys()):
        feature_info[record['feature_id']] = {'lib': record['lib'], 'repo': record['repository'],
                                              'duration': feature_stat_and_duration[record['feature_id']]['duration'], 'Removal Status': feature_stat_and_duration[record['feature_id']]['Removal Status']}
    # +++++++++++++++++++++++++
    if not(record['feature_id'] in duration_vs_CCN.keys()):
        duration_vs_CCN[record['feature_id']] = float(
            record['complexity']['CCN'])
    else:
        duration_vs_CCN[record['feature_id']
                        ] += float(record['complexity']['CCN'])
for feature_id in feature_info:
    df2 = df2.append(pd.DataFrame([[feature_id, duration_vs_CCN[feature_id], feature_info[feature_id]['lib'], feature_info[feature_id]['repo'],
                                    feature_info[feature_id]['duration'], feature_info[feature_id]['Removal Status']]], columns=['id', 'Cyclomatic Complexity Number', 'lib', 'repo', 'duration', 'Removal Status']), ignore_index=True)
g = sns.scatterplot(x="Cyclomatic Complexity Number", y="duration",
                hue="lib", style='Removal Status', data=df2,ax=ax[2])
g.set_ylabel("Duration (Days)",fontsize=15)
ax[2].set_ylabel("Duration (Days)",fontsize=15)

ax[2].legend_.remove()
g = g.set(xlim=(0,40),ylim=(-30,1800))
# --------------------------------------------------------------------------------
# plot for Halstead effort vs complexity
print("Halstead VS life time")
for record in records:
    if not(record['feature_id'] in feature_info.keys()):
        feature_info[record['feature_id']] = {'lib': record['lib'], 'repo': record['repository'],
                                              'duration': feature_stat_and_duration[record['feature_id']]['duration'], 'Removal Status': feature_stat_and_duration[record['feature_id']]['Removal Status']}
    # +++++++++++++++++++++++++
    # print(float(record['complexity']['Halstead effort'].strip()))
    if not(record['feature_id'] in duration_vs_HE.keys()):
        duration_vs_HE[record['feature_id']] = float(
            record['complexity']['Halstead effort'])
    else:
        duration_vs_HE[record['feature_id']
                       ] += float(record['complexity']['Halstead effort'])
for feature_id in feature_info:
    df3 = df3.append(pd.DataFrame([[feature_id, duration_vs_HE[feature_id], feature_info[feature_id]['lib'], feature_info[feature_id]['repo'],
                                    feature_info[feature_id]['duration'], feature_info[feature_id]['Removal Status']]], columns=['id', 'Halstead Effort', 'lib', 'repo', 'duration', 'Removal Status']), ignore_index=True)

g = sns.scatterplot(x="Halstead Effort", y="duration",
                hue="lib", style='Removal Status', data=df3,ax=ax[3])
ax[3].set_ylabel("Duration (Days)",fontsize=15)
ax[3].legend_.remove()
g = g.set(xlim=(0,20000),ylim=(-30,1800))
# -----------------------------------------------------------------------------
# plot maintainability index vs life time
print("maintainability index VS life time")
coutner={}
for record in records:
    if not(record['feature_id'] in feature_info.keys()):
        feature_info[record['feature_id']] = {'lib': record['lib'], 'repo': record['repository'],
                                              'duration': feature_stat_and_duration[record['feature_id']]['duration'], 'Removal Status': feature_stat_and_duration[record['feature_id']]['Removal Status']}
    # +++++++++++++++++++++++++
    # print(float(record['complexity']['Maintainability index'].strip()))
    if not(record['feature_id'] in duration_vs_MI.keys()):
        duration_vs_MI[record['feature_id']] = float(
            record['complexity']['Maintainability index'])
        coutner[record['feature_id']] = 1
    else:
        coutner[record['feature_id']] += 1
        duration_vs_MI[record['feature_id']
                       ] += float(record['complexity']['Maintainability index'])
for feature_id in feature_info:
    df4 = df4.append(pd.DataFrame([[feature_id, duration_vs_MI[feature_id]/coutner[feature_id], feature_info[feature_id]['lib'], feature_info[feature_id]['repo'],
                                    feature_info[feature_id]['duration'], feature_info[feature_id]['Removal Status']]], columns=['id', 'Maintainability Index', 'Librareis:', 'repo', 'duration', 'Removal Status:']), ignore_index=True)

g = sns.scatterplot(x="Maintainability Index", y="duration",
                hue="Librareis:", style='Removal Status:', data=df4,ax=ax[4])
ax[4].set_ylabel("Duration (Days)",fontsize=15)
# ax[4].legend_.remove()
g = g.set(xlim=(0,200),ylim=(-30,1800))
# --------------------------------------------------------------------------------

# plot First-order density vs life time
print("First-order density VS life time")
coutner={}
for record in records:
    if not(record['feature_id'] in feature_info.keys()):
        feature_info[record['feature_id']] = {'lib': record['lib'], 'repo': record['repository'],
                                              'duration': feature_stat_and_duration[record['feature_id']]['duration'], 'Removal Status': feature_stat_and_duration[record['feature_id']]['Removal Status']}
    # +++++++++++++++++++++++++
    # print(float(record['complexity']['First-order density'].strip()))
    if not(record['feature_id'] in duration_vs_FOD.keys()):
        coutner[record['feature_id']] = 1
        # print(record['complexity']['First-order density'])
        duration_vs_FOD[record['feature_id']] = float(
            record['complexity']['First-order density'].replace('%',''))
    else:
        coutner[record['feature_id']] += 1
        duration_vs_FOD[record['feature_id']
                       ] += float(record['complexity']['First-order density'].replace('%',''))


for feature_id in feature_info:
    df5 = df5.append(pd.DataFrame([[feature_id, duration_vs_FOD[feature_id]/coutner[feature_id], feature_info[feature_id]['lib'], feature_info[feature_id]['repo'],
                                    feature_info[feature_id]['duration'], feature_info[feature_id]['Removal Status']]], columns=['id', 'First-order density', 'lib', 'repo', 'duration', 'Removal Status']), ignore_index=True)

# g = sns.scatterplot(x="First-order density", y="duration",
#                 hue="lib", style='Removal Status', data=df5,ax=ax[5])
# # ax[5].legend_.remove()
# g = g.set(xlim=(0,100),ylim=(-30,1800))


#-----------------------------------------------------------------------
#calc Logic LOC
print("Logical LOC VS life time")
for record in records:
    if not(record['feature_id'] in feature_info.keys()):
        feature_info[record['feature_id']] = {'lib': record['lib'], 'repo': record['repository'],
                                              'duration': feature_stat_and_duration[record['feature_id']]['duration'], 'Removal Status': feature_stat_and_duration[record['feature_id']]['Removal Status']}
    # +++++++++++++++++++++++++
    # print(float(record['complexity']['Logical LOC'].strip()))
    if not(record['feature_id'] in duration_vs_LLOC.keys()):
        duration_vs_LLOC[record['feature_id']] = float(
            record['complexity']['Logical LOC'])
    else:
        duration_vs_LLOC[record['feature_id']
                       ] += float(record['complexity']['Logical LOC'])
for feature_id in feature_info:
    df6 = df6.append(pd.DataFrame([[feature_id, duration_vs_LLOC[feature_id], feature_info[feature_id]['lib'], feature_info[feature_id]['repo'],
                                    feature_info[feature_id]['duration'], feature_info[feature_id]['Removal Status']]], columns=['id', 'Logical LOC', 'lib', 'repo', 'duration', 'Removal Status']), ignore_index=True)
g = sns.scatterplot(x="Logical LOC", y="duration",
                hue="lib", style='Removal Status', data=df6,ax=ax[1])
ax[1].legend_.remove()
g = g.set(xlim=(0,150),ylim=(-30,1800))
ax[1].set_ylabel("Duration (Days)",fontsize=15)

ax[4].legend(loc='lower center', bbox_to_anchor=(1.75, -0.2), ncol=2)
plt.show()
# plt.show()
# print(df6)


#-----------------------------------------------------------------------
# calculate Correlations
Corr_df = pd.DataFrame(columns=['LOC', 'Cyclomatic complexity number', 'Halstead effort',
                                'Maintainability index', 'Logical LOC','duration', 'id','Removal Status'])
for id in feature_info:
    # print("id=>", id)
    # print(df1.loc[df1.id == id]["LOC"].values[0])
    # print(df2.loc[df2.id == id]["CCN"].values[0])
    # print(df3.loc[df3.id == id], "Halstead effort")
    # print(df4.loc[df4.id == id], "Maintainability index")
    # print(df5.loc[df5.id == id], "First-order density")
    Corr_df = Corr_df.append(pd.DataFrame([[float(df1.loc[df1.id == id]["LOC"].values[0]), df2.loc[df2.id == id]["Cyclomatic Complexity Number"].values[0], df3.loc[df3.id == id]["Halstead effort"].values[0],
                                            df4.loc[df4.id ==
                                                    id]["Maintainability index"].values[0],
                                            df6.loc[df6.id ==
                                                    id]["Logical LOC"].values[0],
                                            feature_info[id]['duration'],
                                            id,
                                            float(feature_info[id]['Removal Status'])
                                            ]], columns=['LOC', 'Cyclomatic complexity number', 'Halstead effort', 'Maintainability index', 'Logical LOC','duration', 'id','Removal Status']),ignore_index=True)

data_DF = (Corr_df.drop(['id'], axis=1))
corr = data_DF.corr().applymap(lambda x: round(100* x,2)).applymap(str)
keys=['LOC','Cyclomatic complexity number',  'Halstead effort',  'Maintainability index',  'Logical LOC',  'Duration'  ,'Removal Status']
print(corr)
print('&   \\textit{'.join(keys))
for i in range(len(corr)):
    print(keys[i],'&',' & '.join(corr.iloc[i]),'\\\\ \midrule')

    # string = keys[i]
    # for key in keys:
    #     string+=data_DF.iloc[i][key],','
    # print(string)
data_DF = data_DF.loc[data_DF['Removal Status'] == 1]
# print(data_DF.removed == True)
print(data_DF.corr())
print(len(data_DF))
#-------------------------------------------------
exit()
rkf = RepeatedKFold(n_splits=5, n_repeats=10, random_state=2652124)
# data_DF.reset_index(drop=True, inplace=True)
Avg_diff=0
iteration=0
err=[]

for train_index, test_index in rkf.split(data_DF):
    iteration+=1
    print(iteration)
    X_train = (data_DF.iloc[train_index][['LOC', 'Cyclomatic complexity number', 'Halstead effort', 'Maintainability index', 'First-order density']])
    X_test = (data_DF.iloc[test_index][['LOC', 'Cyclomatic complexity number', 'Halstead effort', 'Maintainability index', 'First-order density']])
    y_train = (data_DF.iloc[train_index][['duration']])
    y_test = (data_DF.iloc[test_index][['duration']])
    # print(X_train.values.tolist())
    # print(y_train.values.tolist())
    params={'hidden_layer_sizes':(13,13,13),'max_iter':10000}
    #training
    mlp = MLPClassifier(**params)
    mlp.fit(X_train,y_train.values.ravel())
    y_pred = mlp.predict(X_test)
    tmp=0
    y_test=y_test.values
    # print(y_test)
    for i in range(0, len(y_pred)):
        try:
            newE =abs(y_pred[i]-int(y_test[i][0]))/(y_test[i][0]+y_pred[i])
            print(newE)
            if(math.isnan(newE)):
                print("error!")
                newE=0
            tmp+=newE
            err.append((y_pred[i]-int(y_test[i][0])))
            pass
        except:
            print("error")
            tmp+=0
            pass
    tmp/=len(y_pred)
    Avg_diff+=tmp
    print('result of iter#'+str(iteration)+': '+str(tmp))
    # break;
# sns.boxplot(
    
# )

# fig, axlist = plt.subplots(1, 2)  # , gridspec_kw={'width_ratios': [[1, 3]]})
sns.boxplot(x=err)
plt.show()
print(Avg_diff/(50))