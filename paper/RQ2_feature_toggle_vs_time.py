import pickle
import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import time 
from datetime import datetime
import math
import lizard
import os
import re
import esprima
from react import jsx
from hbs_coverter import convert
from md_coverter import md_convert
from ang_converter import ang_converter
from time import sleep
import jsbeautifier

# transformer = jsx.JSXTransformer()
# js = transformer.transform_string(' if (enableSprintTab) {tabs.push({ id: "sprint", caption: props.t("Sprint") });}')
# print(js)
# exit();
# program='if (enableSprintTab) {tabs.push({ id: "sprint", caption: props.t("Sprint") });}'
# es = esprima.parseScript(program);
# print(es)
# exit();
cnt=0;
ang=0;
ts=0;
tse=0
def detect_language(code_chunk,ext):  
    orig_code=code_chunk
    global cnt  
    global ang 
    global ts
    global tse
    regexp = re.compile(r"<[A-Z]")
    regexp2 = re.compile(r"<[a-zA-Z]")
    regexp3 = re.compile(r"\{\{#((.|\n|\r)*?)\}\}")
    if (ext=='ts' or ext=='tsx'):
        lang='ts'
        ts+=1
    elif (ext=='hbs'):
        lang='hbs'
    elif regexp3.search(code_chunk):
        lang='hbs'
    elif regexp.search(code_chunk):
        lang='jsx'
    elif (ext=='js' or ext=='jsx') and  regexp2.search(code_chunk):
        lang='jsx'
    elif (ext=='html' or ext=='htm'): #it is angular
        lang='angular'
    elif (ext=='vue'):
        lang='ts'
    elif (ext=='md'):
        lang='md'
    else:
        lang='js'

    # print(lang)
    # print(code_chunk)
    if lang=='md':
        try:
            code_chunk1 = md_convert(code_chunk);
            esprima.parseScript(code_chunk1)
            code_chunk= code_chunk1
        except:
            cnt+=1
            print(cnt)
            print(orig_code)
            # lang="hbs-err"
    elif lang=='js':
        try:
            code_chunk = code_chunk.replace("return ","").replace("return;","").replace("@","")
            esprima.parseScript(code_chunk);
        except:
            cnt+=1
            print(orig_code)
            # lang="js-err"
    elif lang=='jsx':
        try:
            code_chunk = code_chunk.replace("return","")
            transformer = jsx.JSXTransformer()
            code_chunk = transformer.transform_string(code_chunk)
            esprima.parseScript(code_chunk);
        except:
            try:
                code_chunk = transformer.transform_string('<div>'+code_chunk+'</div>')
                esprima.parseScript(code_chunk);
            except:                
                cnt+=1
                print(cnt)
                print(orig_code)
                # print(code_chunk);
                # lang="jsx-err"
    elif lang=="hbs":
        try:
            code_chunk1 = convert(code_chunk);
            esprima.parseScript(code_chunk1)
            code_chunk= code_chunk1
        except:
            cnt+=1
            print(cnt)
            print(orig_code)
            # lang="hbs-err"
    elif lang=="angular":
        try:
            code_chunk1 = ang_converter(code_chunk);
            esprima.parseScript(code_chunk1)
            code_chunk = code_chunk1
            pass
        except Exception as e:
            cnt+=1
            print(cnt)
            print(orig_code)
            # lang="ang-err"
    elif lang=="ts":
        try:
            esprima.parseScript(code_chunk.replace("@",""))
        except:
            try:
                code_chunk = code_chunk.replace("return","")
                transformer = jsx.JSXTransformer()
                code_chunk = transformer.transform_string(code_chunk)
                esprima.parseScript(code_chunk);
            except:
                tse+=1
                cnt+=1
                print(cnt)
                print(orig_code)

    return lang,code_chunk

def calculate_complexity(code):
    #first we prettify the code
    code = jsbeautifier.beautify(code)
    #save the content to a file
    f = open('./holder.js', 'w+')
    f.write(code)
    f.close()
    time.sleep(0.02)
    returns=0
    if(code.find('return')!=-1):
        returns = code.count('return')
        code = code.replace('return ', '')
    os.system('cr -e ./holder.js -o ./report-holder.txt ')
    time.sleep(0.02)
    f = open('./report-holder.txt', 'r')
    report = f.read()
    f.close()
    regex = r".*?:(.*)$"
    matches = re.finditer(regex, report, re.MULTILINE)
    metrics=['Logical LOC','param cnt','CCN','Halstead effort','Maintainability index','First-order density','Change cost','Core size']
    metrics.reverse()
    code_metrics={}
    for matchNum, match in enumerate(matches, start=1):
        try:
            code_metrics[metrics.pop()] = match.groups(1)[0]
        except :
            break;
    return code_metrics 

xls = pd.ExcelFile('./complexity/toggles_files.xlsx')
df1 = pd.read_excel(xls, 'life-cycle')
df2 = pd.read_excel(xls, 'Application_of_42_repos')
repo_to_lib={}
# print(df2)
for i in range(0,len(df2)-1):
    if not(df2.iloc[i]['repo'] in repo_to_lib.keys()):
        repo_to_lib[df2.iloc[i]['repo']] =  df2.iloc[i]['lib']
# repos={}
# for i in range(0,len(df1)):
#     if not(df1.iloc[i]['prj'] in repos.keys()):
#         repos[df1.iloc[i]['prj']] = 1
#         print(df1.iloc[i]['prj'])
# print(repo_to_lib.keys())
# exit()
#     print(df2.iloc[i]['repo'])
#     print(df2.iloc[i]['lib'])
#     print('--------------------')
# exit()
# print(df1.iloc[1]['mod1'])
# i = lizard.analyze_file.analyze_source_code("./AllTests.js", df1.iloc[1]['mod1'])
# print(i.__dict__)
# exit();
code_chunks=[]
feature_codes = []
for i in range(0,len(df1)):
    # try:
    ext = df1.iloc[i]['mod1 file'].split('.').pop().lower().strip()
    print('i:',i)
    # print(ext)
    # if(ext=='ts' or ext=='tsx'):
    # print()
    # continue;
    lang,code = detect_language(df1.iloc[i]['mod1'],ext)
    print(lang)
    # print(code)
    complexity = calculate_complexity(code)
    # print(lang)
    # print()
    # print('---------------------------------------')
    # print()
    # feature_codes.append({
    #     'lang':lang,
    #     'js_code':code,
    #     'complexity':complexity
    # })

    # continue;
    # print(i) 
    # try:
    #     df1.iloc[i]['mod1'] = str(df1.iloc[i]['mod1']).strip()
    #     df1.iloc[i]['mod2'] = str(df1.iloc[i]['mod2']).strip()
    #     df1.iloc[i]['mod3'] = str(df1.iloc[i]['mod3']).strip()
    #     df1.iloc[i]['mod4'] = str(df1.iloc[i]['mod4']).strip()
    # except:
    #     pass
    # print(df1.iloc[i]['mod1'])
    # print(1) 
    # print(str(df1.iloc[i]['mod1'])=='nan')
    # # print(len(str(df1.iloc[i]['mod1'])))
    # print(2) 
    # print((str(df1.iloc[i]['mod2']))=='nan')
    # print((str(df1.iloc[i]['mod2']))==' ')
    # print(3) 
    # print((str(df1.iloc[i]['mod3']))=='nan')
    # print((str(df1.iloc[i]['mod3']))==' ')
    # print(4) 
    # print((str(df1.iloc[i]['mod4']))=='nan')
    # print((str(df1.iloc[i]['mod4']))==' ')
    # print('--------------------------------')
    start_time=(df1.iloc[i]['mod1_date'] - datetime(1970, 1, 1)).total_seconds()
    change_no=0;
    if ((str(df1.iloc[i]['mod2']))=='nan'): #no second change
        end_time = (datetime(2020, 4, 17) - datetime(1970, 1, 1)).total_seconds() #time of last checked flag:17 April 2020
        change_no=1;
    elif ((str(df1.iloc[i]['mod2']))==' '): #finished in second change
        end_time = (df1.iloc[i]['mod2_date'] - datetime(1970, 1, 1)).total_seconds()
        change_no=2;
    elif ((str(df1.iloc[i]['mod3']))=='nan'): #no third change
        end_time = (datetime(2020, 4, 17) - datetime(1970, 1, 1)).total_seconds() #time of last checked flag:17 April 2020
        change_no=2;
    elif ((str(df1.iloc[i]['mod3']))==' '): #finished in third change
        end_time = (df1.iloc[i]['mod3_date'] - datetime(1970, 1, 1)).total_seconds()
        change_no=3;
    elif ((str(df1.iloc[i]['mod4']))=='nan'): #no fourth change
        end_time = (datetime(2020, 4, 17) - datetime(1970, 1, 1)).total_seconds() #time of last checked flag:17 April 2020
        change_no=3;
    elif ((str(df1.iloc[i]['mod4']))==' '): #finished in fourth change
        end_time = (df1.iloc[i]['mod4_date'] - datetime(1970, 1, 1)).total_seconds()
        change_no=4;
    else:
        end_time = (datetime(2020, 4, 17) - datetime(1970, 1, 1)).total_seconds() #time of last checked flag:17 April 2020
        change_no=4;
    print({
        'start':start_time,
        'end':end_time,
        'change_no':change_no
    })
    # continue
    # exit()

    # if   len(str(df1.iloc[i]['mod4'])) != 3  or not(math.isnan(df1.iloc[i]['mod4'])):
    #     # print(df1.iloc[i]['mod4_date'])
    #     end_time = (df1.iloc[i]['mod4_date'] - datetime(1970, 1, 1)).total_seconds()
    # elif len(str(df1.iloc[i]['mod3'])) !=3 or not(math.isnan(df1.iloc[i]['mod3'])):
    #     # print(df1.iloc[i]['mod3_date'])
    #     end_time = (df1.iloc[i]['mod3_date'] - datetime(1970, 1, 1)).total_seconds()
    # elif len(str(df1.iloc[i]['mod2'])) != 3 or not(math.isnan(df1.iloc[i]['mod2'])):
    #     # print(df1.iloc[i]['mod2_date'])
    #     end_time = (df1.iloc[i]['mod2_date'] - datetime(1970, 1, 1)).total_seconds()
    # else:
    #     # dateTimeObj = datetime.now()
    #     end_time = (datetime(2020, dateTimeObj.month, dateTimeObj.day) - datetime(1970, 1, 1)).total_seconds()
    duration = ((end_time-start_time)//(60 * 60 * 24))
    if duration <0:
        print(df1.iloc[i]['mod1'])
        print("erorrrrrrrrrrrrr!",i);
    feature_codes.append({
        'id':i,
        'feature_id':df1.iloc[i]['id'],
        'repository':df1.iloc[i]['prj'],
        'start':start_time,
        'end':end_time,
        'complexity': complexity,
        'lang':lang,
        'duration':duration,
        'lib':repo_to_lib[df1.iloc[i]['prj']],
        'LOC': df1.iloc[i]['mod1'].count("\n")
    })
    # print({
    #     'start':start_time,
    #     'end':end_time,
    # })
    # sleep(1)

f = open('./paper/analyze_chunks_final_v5.pckl', 'wb')
pickle.dump(feature_codes, f)
f.close()
print(len(feature_codes))
print(cnt)
