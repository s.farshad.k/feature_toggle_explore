import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt

xls = pd.ExcelFile('./complexity/toggles.xlsx')
df1 = pd.read_excel(xls, 'Application_of_42_repos')

statistics ={}
for i in range(0,len(df1)-1):
    # print()
    lib=df1.iloc[i]['lib'].strip()
    feature_no=int(df1.iloc[i]['feature_no'])

    if lib in statistics.keys():
        statistics[lib].append(feature_no)
    else:
        statistics[lib]=[feature_no]
merge=['launchdarkly-js-client-sdk','ldclient-js','ldclient-node']
statistics['launchdarkly']=[]
for lib in merge:
    statistics['launchdarkly']+= statistics[lib]
    del statistics[lib]
feature_nos=0
for lib in statistics.keys():
    feature_nos+=len(statistics[lib])
print(feature_nos)
unused_library_nos={}
for lib in statistics.keys():
    unused_library_nos[lib]=0
    for val in statistics[lib]:
        if(val ==0):
            unused_library_nos[lib]+=1
print(unused_library_nos)