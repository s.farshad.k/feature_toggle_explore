import pickle
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as mtick
import matplotlib.patches as patches
import matplotlib

filtered_repos= [
'CenterForOpenScience/ember-osf',
'nypublicradio/wnyc-web-client',
'streamlink/streamlink-twitch-gui',
'51ngul4r1ty/atoll-core',
'BulletTrainHQ/bullet-train-frontend',
'aptible/dashboard.aptible.com',
'aymerick/kowa-client',
'bcgov/ppr',
'codebyravi/ember-library',
'datacite/bracco',
'ddicorpo/RecruitInc',
'dtacci/launchdarkly-test',
'ilios/common',
'krispaks/HSEmployee',
'lblod/ember-rdfa-editor',
'lblod/frontend-gelinkt-notuleren',
'slolobdill44/Hamcamp',
'vitorhariel/monitory',
'bcgov/bcrs-business-create-ui',
'cstffx/atlaskit2',
'chpladmin/chpl-website',
'ibroadfo/flowerpot',
'innovate-technologies/control',
'onap/vid',
'sarupbanskota/get-yalla',
'travis-ci/travis-web',
'xaasutility/xsu',
'cds-snc/ircc-rescheduler',
'forestryio/forestry.io',
'arnaudlimbourg/formidable-ui',
'nypublicradio/wqxr-web-client',
'textlint/textlint',
'severinbeauvais/sbc-common-components',
'RCOSDP/RDM-ember-osf-web'];

# f = open('similarity_results.pckl', 'rb')
# f = open('similarity_results_with_name.pckl', 'rb')
# 'CenterForOpenScience/ember-osf=>checked
# 'nypublicradio/wnyc-web-client'=>checked
# 'streamlink/streamlink-twitch-gui'=>?!(gotta check)
f = open('final_retrieved_data.pckl', 'rb')
records = pickle.load(f)

# f = open('similarity_results.pckl', 'rb')
# similarity = pickle.load(f)
f = open('similarity_results_with_name.pckl', 'rb')
similarity = pickle.load(f)
# print(similarity)
#-----------------------------------
# print(similarity)
similar=0
omitted_repos = []
# print(similarity)
# for i in range(len(similarity)):
#     # if(similarity[i]['p1'] > 0.7 or similarity[i]['p2'] > 0.7 ):
#     #     similar+=1
#     if(similarity[i]['p1'] > similarity[i]['p2']):
#         omitted_repos.append(similarity[i]['pair'][0])
#     else:
#         omitted_repos.append(similarity[i]['pair'][1])
#     # print(similarity[i])
#     # print(similarity[i]['p1'],',',similarity[i]['p2'])
# # print(np.unique(omitted_repos))
# # print(len(np.unique(omitted_repos)))
# # print(len(similarity))
# print(len(records))
# keys=records.keys()
# for record in omitted_repos:
#     if(record in records):
#         records.pop(record, None)

# print(len(records))
# f = open('./after_fork_detection_filter_thr60.pckl', 'wb')
# pickle.dump(records, f)
# f.close()
# for repo in similarity:
#     print(repo,'=>',similarity[repo])
# print(similarity);
# print(len(similarity));
# exit()
#-----------------------------------
sims = list(map(lambda x: x*100,list(similarity.values())))
# print(sims)
# exit()
plt.hist(sims, bins = 10)
# plt.gca().set(xlabel='Similarity Percentage', ylabel='# repositories')
plt.gca().set_xlabel("Max. Similarity Percentage With Another Project",fontsize=15)
plt.gca().set_ylabel("# of Repositories",fontsize=15)
plt.grid(axis='y', alpha=0.75)
plt.grid(axis='x', alpha=0.75)
plt.gca().set_xticks(np.arange(0, 100, 10))
fmt = '%.0f%%' # Format you want the ticks, e.g. '40%'
xticks = mtick.FormatStrFormatter(fmt)
plt.gca().xaxis.set_major_formatter(xticks)
# rect = patches.Rectangle((50,100),40,30,linewidth=1,edgecolor='r',facecolor='none')
# matplotlib.text(0.1, 0.9,'matplotlib', ha='center', va='center', transform=plt.gca().transAxes)
# plt.gca().add_patch(rect)
# for i in range(10):
#     plt.text(sims[1][i],sims[0][i],str(sims[0][i]))
# plt.yscale('log')
plt.show()
# ---------------------------------------
# f = open('after_fork_detection_filter.pckl', 'rb')
# records = pickle.load(f)

# f = open('final_repo_names.pckl', 'rb')
# final_repo_names = pickle.load(f)

# file_no=0;
# commit_no=0;
# for repo in final_repo_names:
#     print(records[repo]['record'])
#     # print(records[repo])
#     # print((records[repo]['filesNo'].strip()))
#     file_no+=int(records[repo]['filesNo'].strip("\n"))
#     commit_no+=int(records[repo]['commitsNo'].strip("\n"))
# print(commit_no)
# print(file_no)