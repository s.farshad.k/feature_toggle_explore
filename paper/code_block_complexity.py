from datetime import date
import matplotlib.ticker as ticker
import matplotlib.pyplot as plt
import pandas as pd
import pickle
import numpy as np
import time
from matplotlib import rcParams
import seaborn as sns
sns.set()
sns.set_palette(sns.color_palette('hls', 24))
f = open('./paper/analyze_chunks_final_v5.pckl', 'rb')
records = pickle.load(f)

# print(records[165])
# print(records[166])
# print(records[167])
# exit();
for i in range(0, len(records)):
    # print(record)
    if records[i]['end'] == 1587081600.0:
        records[i]['removed'] = False
        # because of difference between time of data retreival and data analysis
        # records[i]['duration'] -= 14
        if(records[i]['lang'] == 'js-err'):
            records[i]['lang'] = 'js'
    else:
        records[i]['removed'] = True
df1 = pd.DataFrame()
for i in range(0, len(records)):
    df1 = df1.append(pd.DataFrame([[i, records[i]['LOC']+1, records[i]['lang'].upper(), records[i]['repository'],
                                    (records[i]['duration']), records[i]['removed']]], columns=['id', 'Line of Code', 'Language', 'repo', 'duration', 'Removed']), ignore_index=True)
# g = sns.violinplot(x="Language", y="Line of Code", hue="Removed",
                # data=df1, split=True,palette="muted")
# g = g.set(ylim=(,100))
# plt.show()

g = sns.boxplot(x="Language", y="Line of Code", hue="Removed",
                data=df1, palette="muted")
g.set_xlabel('Language',fontsize=15)
g.set_ylabel('Lines of Code',fontsize=15)
g = g.set(ylim=(-5,100))
plt.show()

df2 = pd.DataFrame()
for i in range(0, len(records)):
    if records[i]['lang'].upper() == 'JS':
        # print(records[i]['complexity']['First-order density'])
        df2 = df2.append(pd.DataFrame([[float(records[i]['LOC']+1),
                                        float(records[i]['complexity']['CCN'].strip()),                                        
                                        float(
                                            records[i]['complexity']['Halstead effort']),
                                        float(records[i]['complexity']
                                              ['Maintainability index']),
                                        float(records[i]['complexity']
                                              ['Logical LOC']),      
                                        float(records[i]['duration']),
                                        records[i]['lang'].upper(), records[i]['repository'], float(records[i]['removed'])]], 
                                        columns=[ 'Line of Code','Cyclomatic complexity number','Halstead effort', 'Maintainability index','Logical LOC', 'Duration','Language', 'repo', 'Removal Status']), ignore_index=True)
                                        # ['LOC','Cyclomatic complexity number',  'Halstead effort',  'Maintainability index',  'Logical LOC',  'Duration'  ,'Removal Status']

        # df2 = df2.append(pd.DataFrame([[ float(records[i]['LOC']+1), float(records[i]['complexity']['CCN'].strip()),
        #                                 float(
        #                                     records[i]['complexity']['Halstead effort']),
        #                                 float(records[i]['complexity']
        #                                       ['Maintainability index']),
        #                                 float(records[i]['complexity']
        #                                       ['First-order density']),
        #                                 records[i]['lang'].upper(), records[i]['repository'],
        #                                 float(records[i]['duration']), float(records[i]['removed'])]], columns=[ 'Line of Code','Halstead effort','Cyclomatic complexity','Maintainability index','First-order density', 'Language', 'repo', 'duration', 'Removed']), ignore_index=True)
# print(df2['First-order density'])
# keys=['LOC','Cyclomatic complexity number',  'Halstead effort',  'Maintainability index',  'Logical LOC',  'Duration'  ,'Removal Status']
# df2 = df2.loc[keys]
print(df2.corr())
corr = df2.corr().applymap(lambda x: round(100* x,2)).applymap(str)
print(corr)
keys=corr.keys()
print('&   \\textit{'.join(keys))
for i in range(len(corr)):
    print(keys[i],'&',' & '.join(corr.iloc[i]),'\\\\ \midrule')
g = sns.boxplot(x="Language", y="Line of Code", hue="Removal Status",
                data=df2, palette="muted")
g.set_xlabel('Language',fontsize=15)
g.set_ylabel('Lines of Code',fontsize=15)

# ax[0].legend_.remove()
g = g.set(ylim=(0, 100))
plt.show()
