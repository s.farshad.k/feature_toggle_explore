import pandas as pd
import pickle
import numpy as np
import time
from matplotlib import rcParams
import seaborn as sns; sns.set()
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
from datetime import date
import matplotlib.ticker as mtick
# plt.style.use('seaborn-whitegrid')
cmap = sns.blend_palette(["firebrick", "palegreen"], 25) 




f = open('./paper/analyze_chunks_final_v4.pckl', 'rb')
records=pickle.load(f);
results = {}

#-----------------common section-------------------------------
dates = []
feature_date_per_repository={}
repos=[]
#first get all the change dates
for record in records:
    if not(record['repository'] in results.keys()):
        results[record['repository']] = []
    if not(record['repository'] in feature_date_per_repository.keys()):
        feature_date_per_repository[record['repository']] = []
    feature_date_per_repository[record['repository']].append(record)
    dates.append(record['start'])
    dates.append(record['end'])
dates = sorted(dates)
#-----------------end of common section-------------------------------

#----------------------------------------------------------------
# #plot code chunks per project (works fine!)
# fig = plt.figure()
# ax = plt.gca()
# plot_time = []
# cnt=0
# for date in dates:
#     obj=time.gmtime(date)
#     plot_time.append(str(obj.tm_year)+'/'+str(obj.tm_mon)+'/'+str(obj.tm_mday))
# for repository in feature_date_per_repository.keys():
#     print(repository)
#     repos.append(repository)
#     results[repository] = np.zeros(len(dates))
#     # print(feature_date_per_repository[repository])
#     # exit()
#     for feature_case in feature_date_per_repository[repository]:
#         # print(feature_case['start'])
#         # print('start=>', time.gmtime(feature_case['start']))
#         # print('end=>', time.gmtime(feature_case['end']))
#         # print('----------------------------------------')
#         for i in range(0,len(dates)-1):
#             if( feature_case['start']  == dates[i]):
#                 results[repository][i:] +=1
#                 break
#             if( feature_case['end']  == dates[i]):
#                 results[repository][i:] -=1
#     # print(len(results[repository]))
#     # x = np.linspace(1, len(results[repository]),len(results[repository]))
#     ax=sns.lineplot(x=plot_time, y=results[repository],dashes=[(2,2)]);#, marker='.');
#     if(cnt//10 ==0):
#         ax.lines[cnt].set_linestyle("-")
#         # leg_lines[cnt].set_linestyle("-")
#     elif(cnt//10 ==1):
#         ax.lines[cnt].set_linestyle(":")
#         # leg_lines[cnt].set_linestyle(":")
#     else:
#         ax.lines[cnt].set_linestyle("--")
#         # leg_lines[cnt].set_linestyle("--")
#     cnt+=1
    
    
# ax.set_yscale('log')
# ax = plt.gca()
# sns.set_palette(sns.color_palette('hls', 24))
# leg = ax.legend(labels=repos,loc='lower center', bbox_to_anchor=(0.5, -0.75), ncol=4)
# # leg = ax.legend(labels=repos,loc='center left', bbox_to_anchor=(1.02, 0.5), ncol=1)
# ax.xaxis.set_major_locator(ticker.MultipleLocator(base=10))
# plt.tight_layout()
# ax.set_ylabel("Present Features Flag Code Blocks",fontsize=15)

# fig.autofmt_xdate()
# plt.show()

#-----------------------------------------end of plot--------------

    
#------------------------------------------------------------------        
# #plot feature flags number per project ()
# #first find lifecycle of a feature 

features_life_cycle={} #will have dates for interaction with feature flag
repos_feature_flags ={}
repos=[]
feature_to_repo={}

for repository in feature_date_per_repository.keys():
    repos.append(repository)
    repos_feature_flags[repository] = []
    for feature_case in feature_date_per_repository[repository]:
        if not(feature_case['feature_id'] in feature_to_repo.keys()):
            feature_to_repo[feature_case['feature_id']] = repository
        if not(feature_case['feature_id'] in features_life_cycle.keys()):
            features_life_cycle[feature_case['feature_id']]=[]
            repos_feature_flags[repository].append(feature_case['feature_id'])
        features_life_cycle[feature_case['feature_id']].append(feature_case['start'])
        features_life_cycle[feature_case['feature_id']].append(feature_case['end'])
features_life ={} 

feature_life_dates=[]
for feature_id in features_life_cycle.keys():
    features_life_cycle[feature_id] = sorted(features_life_cycle[feature_id] )
    features_life[feature_id] = [features_life_cycle[feature_id][0],features_life_cycle[feature_id][-1]]
    feature_life_dates.append(features_life_cycle[feature_id][0])
    feature_life_dates.append(features_life_cycle[feature_id][-1])
feature_life_dates = np.unique(feature_life_dates);
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#plot feature no vs change ticks 
# fig = plt.figure()
# ax = plt.axes()
# for repo in repos:
#     results[repo] = np.zeros(len(feature_life_dates))
#     # fig = plt.figure()
#     # ax = plt.axes()
#     for feature_id in repos_feature_flags[repo]:
#         #we have feature_id, repo and feature_life of feature_id
#         print("repos:",repo,"-feature_id:",feature_id)
#         print('start=>', features_life[feature_id][0])
#         print('end=>', features_life[feature_id][1])
#         print("----------------------------")
#         for i in range(0,len(feature_life_dates)-1):
#             if( features_life[feature_id][0]  == feature_life_dates[i]):
#                 results[repo][i:] +=1
#                 break
#             if( features_life[feature_id][1]  == feature_life_dates[i]):
#                 results[repo][i:] -=1
#     x = np.linspace(1, len(results[repo]),len(results[repo]))
#     ax.plot(x, results[repo]);
# plt.show()
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
fig = plt.figure()
plot_time = []
cnt=0
for date in feature_life_dates:
    obj=time.gmtime(date)
    plot_time.append(str(obj.tm_year)+'/'+str(obj.tm_mon)+'/'+str(obj.tm_mday))
# print((repos))
for repo in repos:
    results[repo] = np.zeros(len(feature_life_dates))
    # print(len(repos_feature_flags[repo]))
    # print("repos:",repo)
    for feature_id in repos_feature_flags[repo]:
        #we have feature_id, repo and feature_life of feature_id
        # print("repos:",repo,"-feature_id:",feature_id)
        # print('start=>', time.gmtime(features_life[feature_id][0]))
        # print('end=>', time.gmtime(features_life[feature_id][1]))
        # print("----------------------------")
        for i in range(0,len(feature_life_dates)-1):
            if( features_life[feature_id][0]  == feature_life_dates[i]):
                # print(time.gmtime(feature_life_dates[i]))
                results[repo][i:] +=1
                break
        for i in range(0,len(feature_life_dates)-1):
            if( features_life[feature_id][1]  == feature_life_dates[i]):
                # print(time.gmtime(feature_life_dates[i]))
                results[repo][i:] -=1
                break;
        # print(results)
    x = np.linspace(1, len(results[repo]),len(results[repo]))
    ax=sns.lineplot(x=plot_time, y=results[repo],dashes=[(2,2)]);#, marker='.');
    if(cnt//10 ==0):
        ax.lines[cnt].set_linestyle("-")
        # leg_lines[cnt].set_linestyle("-")
    elif(cnt//10 ==1):
        ax.lines[cnt].set_linestyle(":")
        # leg_lines[cnt].set_linestyle(":")
    else:
        ax.lines[cnt].set_linestyle("--")
    cnt+=1
        # leg_lines[cnt].set_linestyle("--")
    # sns.lineplot(x=plot_time, y=results[repo]);#, marker='.');


ax = plt.gca()
# sns.set_palette(sns.color_palette('hls', 24))
leg = ax.legend(labels=repos,loc='lower center', bbox_to_anchor=(0.5, -0.75), ncol=4)
ax.xaxis.set_major_locator(ticker.MultipleLocator(base=10))
plt.tight_layout()
ax.set_ylabel("Present Feature Flags",fontsize=15)

fig.autofmt_xdate()
plt.show()

# exit()
# ax = plt.gca()
# sns.set_palette(sns.color_palette('hls', 24))
# leg = ax.legend(labels=repos,loc='center left', bbox_to_anchor=(1.02, 0.5), ncol=2)
# ax.xaxis.set_major_locator(ticker.MultipleLocator(base=10))
# plt.tight_layout()
# fig.autofmt_xdate()
# plt.show()

#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#--------------------------------------------------------------------------
# print(features_life_cycle)
remained =[]
removed_ids = list(features_life_cycle.keys())
print(len(removed_ids))
for feature_id in features_life_cycle:
    if(features_life_cycle[feature_id][-1] == 1587081600.0):
        remained.append(feature_id)
for f_id in remained:
    removed_ids.remove(f_id)

durations = []
duration_per_repo = {}

feature_no_per_repo={}
remove_feature_no_per_repo={}
all_duration_per_repo ={}
for feature_id in features_life_cycle:
    if not(feature_to_repo[feature_id] in feature_no_per_repo.keys()):
        feature_no_per_repo[feature_to_repo[feature_id]]=1
    else:
        feature_no_per_repo[feature_to_repo[feature_id]]+=1
    duration = (features_life_cycle[feature_id][-1] - features_life_cycle[feature_id][0])//(60 * 60 * 24)
    if not(feature_to_repo[feature_id] in all_duration_per_repo.keys()):
        all_duration_per_repo[feature_id] = {'duration':duration,'removed':feature_id in removed_ids}

    if not(feature_id in removed_ids):
        continue;
    if not(feature_to_repo[feature_id] in remove_feature_no_per_repo.keys()):
        remove_feature_no_per_repo[feature_to_repo[feature_id]]=1
    else:
        remove_feature_no_per_repo[feature_to_repo[feature_id]]+=1


    duration = (features_life_cycle[feature_id][-1] - features_life_cycle[feature_id][0])//(60 * 60 * 24)
    durations.append(duration)
    if not(feature_to_repo[feature_id] in duration_per_repo.keys()):
        duration_per_repo[feature_to_repo[feature_id]] = [duration]
    else:
        duration_per_repo[feature_to_repo[feature_id]].append(duration)
# f = open('./feature_status_and_duration_v2.pckl', 'wb')
# pickle.dump(all_duration_per_repo, f)
# print(all_duration_per_repo)
f.close()
# exit()
removed_percentage=[]
for repo in feature_no_per_repo:
    # print(repo,"=>",feature_no_per_repo[repo])
    if not(repo in remove_feature_no_per_repo.keys()):
        remove_feature_no_per_repo[repo]=0
    # print(repo,"=>",remove_feature_no_per_repo[repo])
    removed_percentage.append(remove_feature_no_per_repo[repo]/feature_no_per_repo[repo]*100)
print(removed_percentage)
g= sns.distplot(removed_percentage, kde=False,bins=10);
# g.set(xlabel='% Feature flags removed', ylabel='# Repositories')
g.set_xlabel('Percentage of Removed Feature Flags',fontsize=15)
g.set_ylabel('# of Repositories',fontsize=15)
fmt = '%.0f%%' # Format you want the ticks, e.g. '40%'
xticks = mtick.FormatStrFormatter(fmt)
plt.gca().xaxis.set_major_formatter(xticks)

plt.show()
# exit()
    
df=pd.DataFrame(columns=["duration (days)","repository"])
for repo in duration_per_repo:
    for duration in duration_per_repo[repo]:        
        df = df.append( pd.DataFrame([[duration,repo]],columns=["duration (days)","repository"]), ignore_index=True)

print(df)
# exit()
f, axes = plt.subplots(1, 2, gridspec_kw={'width_ratios': [1, 3]})
g = sns.stripplot(x="repository", y="duration (days)", data=df,ax=axes[1])
g.set(xticklabels=[])
g.set_xlabel("Repositories",fontsize=15)
g.set_ylabel('Duration (days)',fontsize=15)

# sns.factorplot(data=duration_per_repo)

# ax = plt.gca()
sns.set_palette(sns.color_palette('hls', 24))


# ax.xaxis.set_major_locator(ticker.MultipleLocator(base=10))
# plt.show()


print(np.average(durations))
print(np.median(durations))
g = sns.boxplot(y=durations,ax=axes[0]);
# g.set(xlabel='Removed Feature Flags', ylabel='Duration (days)')
g.set_xlabel('Removed Feature Flags',fontsize=15)
g.set_ylabel('Duration (days)',fontsize=15)

leg = axes[1].legend(labels=repos,loc='center left', bbox_to_anchor=(1.02, 0.5), ncol=1)  
# axes[1].set_xlabel('')
plt.show()

f, axes = plt.subplots(1, 2, gridspec_kw={'width_ratios': [1, 1]})
g= sns.distplot(durations,ax=axes[1])
g.set(xlabel='Duration (days)', ylabel='')

g= sns.distplot(durations,ax=axes[1])
g.set(xlabel='Duration (days)', ylabel='')
plt.show()
#--------------------------------------------------------------------------
