import re
regex = r"{{([^\/~])((.|\n)*?)}}"
debug=False;

def convert(code,checked_tags=[],level=0):
    code = code.replace("{{{","{{").replace("}}}","}}")
    # code = re.sub('<[^\/].*?>','document.write("sth")',code)

    #first we replace everything in between with a simple function
    if debug:
        print('level:',level)
    newParts=[]
    matches = re.finditer(regex, code, re.MULTILINE)
    for matchNum, match in enumerate(matches, start=1):   
        groupNum=2
        rplc_str = '{{'+match.group(1)+match.group(groupNum)+'}}'
        parts = match.group(groupNum).replace('\n',' ').split(' ');
        if(match.group(1)=='#'):
            func = parts[0]
        else:
            func = match.group(1)+parts[0]
        if debug:
            print(func)
        atr=''
        if len(parts)>1:
            atr=''
            for i in range(1,len(parts)):
                if len(parts[i].strip())!=0:
                    atr +=  parts[i].strip()+','
            atr= atr.strip(',').replace("'",'"');
        if func=='if' or func=="unless":
            func='if'
            if(level==0):
                if(match.group(1)!='#'):
                    func_regex = '(<.*?{{'+match.group(1)+match.group(groupNum).replace('.','\.').replace('(','\(').replace(')','\)')+'}}.*?>)'
                    # print(func_regex)
                    # print(code)
                    code = re.sub(func_regex, 'if(a){document.write("sth");}else{document.write("some other thing")}', code)
                    # print(code)
                    newParts.append('if(a){document.write("sth");}else{document.write("some other thing")}')
                else:
                    code = code.replace(rplc_str,func+'(some_var){')
                    newParts.append(func+'(some_var){')
            else:
                # if(match.group(1)!='#'):
                #     func_regex = '(<.*?{{'+match.group(1)+match.group(groupNum)+'}}.*?>)'
                #     code = re.sub(func_regex, 'if(a){document.write("sth");}else{document.write("some other thing")}', code)
                #     newParts.append('if(a){document.write("sth");}else{document.write("some other thing")}')
                # else:
                code = code.replace(rplc_str,'(some_var?"this str":"that str")')
                newParts.append('(some_var?"this str":"that str")')
        elif func=='else':
            code = code.replace(rplc_str,'}'+func+'{')
            newParts.append('}'+func+'{')

        elif func=="each":
            if(level==0):
                code = code.replace(rplc_str,'for(var i in sth){')
                newParts.append('for(var i in sth){')
            else:
                code = code.replace(rplc_str,'(some_var?"this str":"that str")')
                newParts.append('(some_var?"this str":"that str")')
        elif func=="is-empty":
            pass
        elif func=="is-empty":
            pass
        else:
            if(code.count('{{/'+func.strip()+'}}')!=0):
                #analyze inside code
                func_regex = '{{'+match.group(1)+match.group(groupNum).replace('.','\.').replace('(','\(').replace(')','\)')+'}}((.|\n|\r)*?){{\/'+func.strip()+'(.*?)}}'
                if debug:
                    print(func_regex)
                    print(func)
                    print('{{/'+func.strip()+'}}')
                    print(code.count('{{/'+func.strip()+'}}'))
                inside_matches = re.finditer(func_regex, code, re.MULTILINE)
                for  inside_matchNum,inside_match in enumerate(inside_matches, start=1):
                    inside_groupNum=1
                    if(str(inside_match.group(inside_groupNum))!='None'):
                        inside_replacement = convert(inside_match.group(inside_groupNum),checked_tags,level+1)
                        code = re.sub(func_regex,func.replace('~','').strip()+"('"+atr+"',"+inside_replacement+")",code)
                        newParts.append(func.replace('~','').strip()+"('"+atr+"',"+inside_replacement+")")
                        break;
                    else:
                        code = code.replace(rplc_str,func+"('"+atr+"')")
                        # code = re.sub(func_regex,func.replace('~','').strip()+"('"+atr+"')",code)
                        newParts.append(func.replace('~','').strip()+"('"+atr+"')")
                        break
            else:
                code = code.replace(rplc_str,func+"('"+atr+"')")
                newParts.append(func.replace('~','')+"('"+atr+"')")
            checked_tags.append(func)    
        if debug:
            print(code)
    close_bracket=False
    if(code.count('{{/unless}}')!=0):
        if(level==0):
            code = code.replace('{{/unless}}','}')
            close_bracket=True
        else:
            code = code.replace('{{/unless}}','')
    if(code.count('{{/if}}')!=0):
        if(level==0):
            code = code.replace('{{/if}}','}')
            close_bracket=True
        else:
            code = code.replace('{{/if}}','')
    if(code.count('{{/each}}')!=0):      
        if(level==0):
            code = code.replace('{{/each}}','}')
            close_bracket=True
        else:
            code = code.replace('{{/each}}','')
    if(code.count('{{~/each}}')!=0):      
        if(level==0):
            code = code.replace('{{~/each}}','}')
            close_bracket=True
        else:
            code = code.replace('{{~/each}}','')

    if close_bracket!=False:
        newParts.append('}')
    #clear any remaining
    code = re.sub('\{\{(.*?)\}\}','',code)


    if debug:
        print(code)

    i=0
    for newPart in newParts:
        code = code.replace(newPart,'[*--'+str(i)+'--#]')
        i+=1;
    # if level!=0:
    #     print(code)
    html_regex = r"\[\*--(\d)*--#]"
    html_matches = re.finditer(html_regex, code, re.MULTILINE)
    new_code='';
    for html_matchNum, html_match in enumerate(html_matches, start=1):
        new_code += html_match.group()+"\n"
    if debug:
        print(new_code)
    code=new_code        
    if debug:
        if level!=0:
            print(code)
    i=0
    for newPart in newParts:
        if level==0:
            code = code.replace('[*--'+str(i)+'--#]',newPart)
        else:
            code = code.replace('[*--'+str(i)+'--#]',newPart+',')
        i+=1;
    code=code.strip(',')
    if debug:
        if level!=0:
            print(code)
    #now we eliminate the html tags and replace them
    # code = re.sub('<[^\/].*?>','document.write("sth")',code)
    # tag_striper(code);
    if(level==0):
        code = re.sub(",\)",")",code)
    return code.strip().strip(',');
# print(convert('''{{#if authorization.features.engines.risk}}
#             {{#link-to 'risk-assessments' class="panel-link"}}
#               {{compliance-engine-status status=model.risk class="risk-engine-status"}}
#             {{/link-to}}
#           {{else}}
#             {{#link-to-aptible app="compliance" path="risk" class="panel-link"}}
#               {{compliance-engine-status status=model.risk class="risk-engine-status"}}
#             {{/link-to-aptible}}
#           {{/if}}'''))
# print(convert(
#     '''{{#if authorization.features.spd}}
#   <ul class="nav sidebar-nav">
#     <li class="title">Security Program</li>
#     {{#each securityProgramSteps as |step|}}
#       <li class="{{if step.current 'active'}}">
#         {{link-to step.name step.path}}
#       </li>
#     {{/each}}
#   </ul>
# {{/if}}'''))
# print(convert(
#     '''{{#if complianceStatus.authorizationContext.enabledFeatures.spd}}
#   <section class="compliance-section {{if complianceStatus.requiresSPD 'spd-required' 'spd-complete'}}">
#     <header>
#       <span class="header-title">Security Program</span>
#     </header>
 
#     <div class="row">
#       {{#each securityProgramSteps as |step|}}
#         <div class="col-xs-6">
#           {{#link-to step.path class="panel-link"}}
#             <div class="panel panel-default security-program-step resource-list-item">
#               <div class="panel-heading">
#                 <h5>{{step.name}}</h5>
#               </div>
#               <div class="panel-body">
#                 {{step.description}}
#               </div>
#              </div>
#           {{/link-to}}
#         </div>
#       {{/each}}
#     </div>
#   </section>
# {{/if}}
#     '''
# ))
# print(convert('''
# {{#if storageI18nEnabled}}
#                     <div class="form-group m-v-md">
#                         <label class="f-w-lg text-bigger" local-class="NewProject__label">
#                             {{t 'new_project.storage_region'}}
#                             {{#power-select
#                                 options=regions
#                                 renderInPlace=true
#                                 selected=selectedRegion
#                                 searchField='name'
#                                 onchange=(action 'selectRegion')
#                                 noMatchesMessage=(t 'new_project.no_matches')
#                                 as |region|
#                             }}
#                                 {{region.name}}
#                             {{/power-select}}
#                         </label>
#                     </div>
#                 {{/if}}
#                         '''))