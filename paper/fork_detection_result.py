import pickle
import numpy as np
from itertools import combinations

f = open('final_retrieved_data.pckl', 'rb')
records = pickle.load(f)
print(len(records))
# exit()
commit_obj = {}
for repo in records:
    # print(records[repo]['commits'])
    commits = records[repo]['commits'].strip().split("\n")
    commit_obj[repo] = []
    for commit in commits:
        commit_obj[repo].append({
            'hash': commit[0:7],
            'msg': commit[8:]
        })

threshold = 0.6
similarity_list = {}
# create unique pairs
repo_list = []
for repos in records.keys():
    repo_list.append(repos)
pair_list = [('codebyravi/ember-library','streamlink/streamlink-twitch-gui')]#[comb for comb in combinations(repo_list, 2)]
cnt = 0
sim_pair=[]
for pair in pair_list:
    cnt += 1
    same_commits = 0
    all_commits = 0
    repo1 = pair[0]
    repo2 = pair[1]
    fork_info = []
    print(str(cnt)+'/'+str(len(pair_list)) +" - comparing {} and {}:".format(repo1, repo2), end="\r")
    precentage1 = int(records[repo1]['commitsNo'])
    precentage2 = int(records[repo2]['commitsNo'])
    print(precentage1)
    print(precentage2)
    for commits1 in commit_obj[repo1]:
        for commits2 in commit_obj[repo2]:
            # print(commits1['hash'],'==',commits2['hash'])
            all_commits += 1
            if commits1['hash'] == commits2['hash']:
                same_commits += 1
    precentage1 = same_commits/precentage1
    precentage2 = same_commits/precentage2
    print(all_commits)
    if precentage1 > threshold:
        if not(repo1 in similarity_list.keys()):
            similarity_list[repo1] = precentage1
        else:
            similarity_list[repo1] = max(precentage1,similarity_list[repo1])
            

    if precentage2 > threshold:
        if not(repo2 in similarity_list.keys()):
            similarity_list[repo2] = precentage2
        else:
            similarity_list[repo2] = max(precentage2,similarity_list[repo2])
    # if precentage1 >threshold or precentage2 >threshold:
    sim_pair.append({
        'pair':pair,
        'p1':precentage1,
        'p2':precentage2,
    })
    # similarity_list.append(precentage1)
    # similarity_list.append(precentage2)
    # if(precentage1 > threshold and precentage2 > threshold):
    #     omission_list.append([{'repo':repo1,'precentage':precentage1},{'repo':repo2,'precentage':precentage2}])
    #     f = open('./fork_detection_result.pckl', 'wb')
    #     pickle.dump(omission_list, f)
    #     f.close()
    #     print(len(omission_list))

print(sim_pair)
# import pickle
# import numpy as np
## f = open('./similarity_results_with_name_edited.pckl', 'wb')
## pickle.dump(similarity_list, f)
## f.close()
## f = open('./similarity_results_pairs.pckl', 'wb')
## pickle.dump(sim_pair, f)
## f.close()

# f = open('similarity_result   s.pckl', 'rb')
# results = pickle.load(f)

# print(results)
# all_similarities = []
# for pair in results:
#     all_similarities.append(pair[0]['precentage'])
#     all_similarities.append(pair[1]['precentage'])
# print(all_similarities)