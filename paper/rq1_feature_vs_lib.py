import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import matplotlib


# df = pd.DataFrame(np.random.randn(10, 4),
#                   columns=['Col1', 'Col2', 'Col3', 'Col4'])
# print(df)
# boxplot = df.boxplot(column=['Col1', 'Col2', 'Col3'])
# exit()
xls = pd.ExcelFile('./complexity/toggles_files.xlsx')
df1 = pd.read_excel(xls, 'Application_of_42_repos')
# print(df1)
statistics ={}
unused_repos={}
for i in range(0,len(df1)-1):
    # print()
    lib=df1.iloc[i]['lib'].strip()
    feature_no=int(df1.iloc[i]['feature_no'])
    if(feature_no==0):
        if(lib in unused_repos):
            unused_repos[lib]+=1
        else:
            unused_repos[lib]=1
    if lib in statistics.keys():
        statistics[lib].append(feature_no)
    else:
        statistics[lib]=[feature_no]
merge=['launchdarkly-js-client-sdk','ldclient-js','ldclient-node']
statistics['launchdarkly']=[]
unused_repos['launchdarkly']=0
for lib in merge:
    if(lib in unused_repos.keys()):
        unused_repos['launchdarkly']+= unused_repos[lib]
        del unused_repos[lib]
for lib in merge:
    statistics['launchdarkly']+= statistics[lib]
    del statistics[lib]
feature_nos={}
for lib in statistics.keys():
    feature_nos[lib]=len(statistics[lib])
print(unused_repos)
# exit()
# print(statistics)
# print(feature_nos)



#-------------------------------------------
#histogram the number of feature flags per library
df = pd.DataFrame({'Libraries':list(feature_nos.keys()), '# of Repositories':list(feature_nos.values())})
df.sort_values(by=['# of Repositories'], inplace=True)
print(df);
# exit()
matplotlib.rcParams.update({'font.size': 15})
ax = df.plot.barh(x='Libraries', y='# of Repositories')
# y_pos = np.arange(len(feature_nos.keys()))
# plt.bar(y_pos, feature_nos.values(), align='center', alpha=0.5)
# plt.xticks(y_pos, feature_nos.keys())
# plt.yticks(rotation=15)
# plt.ylabel('Feature Flags Number')
# plt.title('Library Name')
plt.show()
# exit()
#-------------------------------------------
#plot box two most popular libraries
data=[statistics['launchdarkly']+statistics['ember-feature-flags']]
df = pd.DataFrame(list(zip(*data)),
                  columns=['Feature flag number'])
df['Libraries'] = pd.Series(['launchdarkly']*len(statistics['launchdarkly'])+['ember-feature-flags']*len(statistics['ember-feature-flags']))
# print(df.groupby('Libraries'))
# exit()
sns.set_style("darkgrid", {"axes.facecolor": ".9"})
ax = sns.boxplot(x="Libraries", y="Feature flag number", data=df)
# df.groupby('Libraries')
# boxplot = df.boxplot(by='Libraries',label)
# boxplot.ylabel('Feature Flags Number')

plt.show()


# sns.set(style="whitegrid")
# ax = sns.boxplot(x="day", y="total_bill", data=tips)