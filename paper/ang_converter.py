from time import sleep
try: 
    from BeautifulSoup import BeautifulSoup
except ImportError:
    from bs4 import BeautifulSoup
debug=False
def ang_converter(code,level=0):
    sleep(1)
    # print('level:',level)
    # print('code:',code)
    # print('len:',type(code))

    # parsed_html = BeautifulSoup(html)
    # print(parsed_html.body.find('div', attrs={'class':'container'}).text)
    # print(soup.prettify())
    # print(list(soup.children))
    whole_code=[]
    if(len(code)):
        soup = BeautifulSoup(code, 'html.parser')
        results =soup.find_all(attrs={"feature-flag":True},recursive=False)
        if(len(results)):
            for res in results:
                if(debug):
                    print('res',res)
                start_code="if(some_feature){"
                end_code="}"
                try:
                    if(res['ng-if']):
                        start_code="if(some_if_var){"+start_code
                        end_code+="}"
                except :
                    pass
                try:
                    if(res['ngif']):
                        start_code="if(some_if_var){"+start_code
                        end_code+="}"
                except :
                    pass

                children=res.findChildren(recursive=False)            
                child_codes=[]
                for child in children:
                    child_codes.append(ang_converter(str(child),level+1))
                new_code=''.join(child_codes);
                if(len(new_code)==0):
                    new_code="document.write('html')"
                whole_code.append(start_code+new_code+end_code);
        return ';'.join(whole_code);
    else:
        if debug:
            print('level:',level)
            print('code:',code)
            print('len:',type(code))
        return code;
    # results =soup.find_all(attrs={"feature-flag":True})
    # if(len(results)):
    #     for res in results:
    #         start_code="if(some_feature){"
    #         end_code="}"
    #         children=res.findChildren(recursive=False)
    #         # print(children)
    #         for child in children:
    #             code=ang_converter(code,level+1)
    #             whole_code.append(start_code+code+end_code);
        
# print(ang_converter('''
# <div class="product-element removed" ng-if="true" id="listing-information-transparency-attestation" feature-flag="effective-rule-date">
#               <button class="btn btn-link btn-xs pull-right" uib-tooltip="This requirement has been removed from the Program." tooltip-trigger="'mouseenter focus click'"><span class="sr-only">This requirement has been removed from the Program.</span><i class="fa fa-info-circle fa-lg"></i></button>
#               <h2 feature-flag="effective-rule-date" class="product-heading">Transparency Attestation</h2>
#               <p>{{ $ctrl.listing.transparencyAttestation }}</p>
#              </div>
# '''))