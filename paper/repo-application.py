apps=[['51ngul4r1ty/atoll-core',5],
['51ngul4r1ty/atoll-shared',3],
['BulletTrainHQ/bullet-train-frontend',2],
['CenterForOpenScience/ember-osf',1],
['RCOSDP/RDM-ember-osf-web',1],
['UpdraftGroup/CapitolZen-Frontend',2],
['aptible/dashboard.aptible.com',6],
['arnaudlimbourg/formidable-ui',3],
['aymerick/kowa-client',1],
['bcgov/ppr',1],
['cds-snc/ircc-rescheduler',1],
['codebyravi/ember-library',3],
['datacite/bracco',2],
['ddicorpo/RecruitInc',5],
['dtacci/launchdarkly-test',1],
['forestryio/forestry.io',1],
['hmcts/rpx-xui-manage-organisations',5],
['hmcts/rpx-xui-webapp',1],
['ilios/common',3],
['krispaks/HSEmployee',1],
['lblod/ember-rdfa-editor',3],
['lblod/frontend-gelinkt-notuleren',2],
['lchjczw/gitter_webapp',1],
['newjersey/career-network',1],
['nypublicradio/wnyc-web-client',2],
['nypublicradio/wqxr-web-client',2],
['slolobdill44/Hamcamp',1],
['textlint/textlint',3],
['vitorhariel/monitory',7],
['zotoio/github-task-manager',4],
['bcgov/bcrs-business-create-ui',2],
['chpladmin/chpl-website',2],
['cstffx/atlaskit2',3],
['hmcts/rpx-xui-common-lib',3],
['ibroadfo/flowerpot',1],
['innovate-technologies/control',1],
['onap/vid',4],
['sarupbanskota/get-yalla',2],
['severinbeauvais/sbc-common-components',3],
['streamlink/streamlink-twitch-gui',2],
['travis-ci/travis-web',4],
['xaasutility/xsu',1],
['azu/monorepo-utils',3]]

topics=[
'web app',
'GUI/front-end',
'Framework/Library',
'Development tool',
'Communication/collaboration tool',
'Devops',
'Mobile application',
]
stats = {}
for app in apps:
    print(topics[app[1]-1])
    if topics[app[1]-1] in stats.keys():
        stats[topics[app[1]-1]]+=1
    else:
        stats[topics[app[1]-1]]=1
print(stats)