import pickle
import subprocess
from time import sleep
import numpy as np
from collections import Counter
from itertools import combinations
import matplotlib.pyplot as plt
import scipy.stats as ss
from scipy.stats import gaussian_kde
import seaborn as sns
import pylab as pl
import requests
import sys
plt.style.use('seaborn-whitegrid')
commit_thr=74
file_thr=80
final_commit_thr=74
final_repo_names=[]
# file
# --------------------------------------------------------------------------------
# f = open('after_fork_detection_filter.pckl', 'rb') #thr 70
f = open('after_fork_detection_filter_thr60.pckl', 'rb')
records = pickle.load(f)
# print(len(records))
# exit()
repo_list = []
for repos in records.keys():
    repo_list.append(repos)

#--------------------------------------------------------------------------------
#plot commit number and file number
commit_numbers = []
file_numbers = []
print(len(repo_list))
print('===========================')
survived_repos=[]
for repo in repo_list:
    commitNo=int(records[repo]['commitsNo'].strip())
    commit_numbers.append(commitNo)
    if(final_commit_thr!=-1 and commitNo>=final_commit_thr):
        fileNo=int(records[repo]['filesNo'].strip())
        file_numbers.append(fileNo)
        print(repo)
        survived_repos.append(repo)
print(len(survived_repos))
#threshold testing
print('-------------------------')
passed_filter={}
for repo in repo_list:
    commitNo=int(records[repo]['commitsNo'].strip())
    fileNo=int(records[repo]['filesNo'].strip())
    if commitNo > commit_thr and fileNo > file_thr:
        passed_filter[repo] = records[repo] 
        final_repo_names.append(repo)
        print(repo)
        # print(passed_filter[repo]['record']['path'])
        # print(passed_filter[repo]['record']['lib'])
# print(passed_filter['51ngul4r1ty/atoll-core']['record'])
# f = open('./final_repo_names.pckl', 'wb')
# pickle.dump(final_repo_names, f)
# f.close()
print(len(passed_filter))
# exit();        

libBar={}
all_files = 0
all_commits= 0
for repo in passed_filter:
    # print()
    all_files+=int(passed_filter[repo]['filesNo'])
    all_commits+=int(passed_filter[repo]['commitsNo']
)    # exit();
    if passed_filter[repo]['record']['lib'] in libBar.keys():
        libBar[passed_filter[repo]['record']['lib']]+=1
    else:
        libBar[passed_filter[repo]['record']['lib']]=1
    # all_files = 
print(libBar)
print(str(all_files))
print(str(all_commits))
# import xlsxwriter 
# workbook = xlsxwriter.Workbook('library_uses.xlsx') 
# worksheet = workbook.add_worksheet() 

# index=1
# for lib in libBar.keys():    
#     # folder_name=repo.replace("/",'-')
#     worksheet.write('A{}'.format(index), lib) 
#     worksheet.write('B{}'.format(index), libBar[lib]) 
#     index+=1

# plt.bar(np.arange(len(libBar.keys())), libBar.values())
# plt.xticks(np.arange(len(libBar.keys())), libBar.keys())
# plt.show()
# # ()
# workbook.close() 

#commits
commit_numbers = np.array(commit_numbers)
survived_repos=[]
indexes=[]
thresholds=range(20,100,20)
survived_repos_thr={}

ax = sns.distplot(commit_numbers,  hist = False, kde = True,
             bins=int(203/10), color = 'darkblue', 
             hist_kws={'edgecolor':'black'},
             kde_kws={'shade': True,'linewidth': 1})
# plt.xscale('log')
ax.set_xlabel("# of Commits",fontsize=15)
ax.set_ylabel("Probability Density Function",fontsize=15)
# ax.set(xlabel='', ylabel='')
plt.show()
# print(len(file_numbers))
ax = sns.distplot(file_numbers,  hist = False, kde = True,
             bins=int(205/5), color = 'darkblue', 
             hist_kws={'edgecolor':'black'},
             kde_kws={'shade': True,'linewidth': 1})
# plt.xscale('log')
ax.set_xlabel("# of Files",fontsize=15)
ax.set_ylabel("Probability Density Function",fontsize=15)
plt.show()
# exit()

# all_files=0
for i in range(np.min(commit_numbers),np.max(commit_numbers)+1):
    indexes.append(i+1)
    survived_repos.append(len(np.where(commit_numbers >= i)[0]))
    if( len(np.where(commit_numbers >= i)[0]) == 50):
        print("i=>>>>>>>>>>>>", str(i));
    if(i  in thresholds):
        survived_repos_thr[i] =  len(np.where(commit_numbers >= i)[0])
print("commits Thresholds=>",survived_repos_thr)

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)

major_ticks = np.arange(0, 221, 20)
minor_ticks = np.arange(0, 221, 5)
x_major_ticks = np.arange(0, np.max(commit_numbers), 1000)


ax.set_xticks(x_major_ticks)
ax.set_yticks(major_ticks)
ax.set_yticks(minor_ticks, minor=True)

# And a corresponding grid
ax.grid(which='both')
ax.grid(which='minor', alpha=0.2)
ax.grid(which='major', alpha=0.5)

plt.grid(True)
# ax = plt.axes()
ax.plot(indexes, survived_repos);
ax.set_xscale('log')
ax.set_xlabel("Threshold (# of Commits)",fontsize=15)
ax.set_ylabel("# of Repositories",fontsize=15)
plt.show()

#file numbers
file_numbers = np.array(file_numbers)
survived_repos=[]
indexes=[]
thresholds=range(20,600,20)
survived_repos_thr={}

print('->>>>>>>>>>>',len(file_numbers))
for i in range(np.min(file_numbers),np.max(file_numbers)+1):
    indexes.append(i+1)
    survived_repos.append(len(np.where(file_numbers >= i)[0]))

    if(i  in thresholds):
        survived_repos_thr[i] =  len(np.where(file_numbers >= i)[0])
print("File number Thresholds=>",survived_repos_thr)

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)

major_ticks = np.arange(0, 221, 20)
minor_ticks = np.arange(0, 221, 5)
x_major_ticks = np.arange(0, np.max(file_numbers), 1000)


ax.set_xticks(x_major_ticks)
ax.set_yticks(major_ticks)
ax.set_yticks(minor_ticks, minor=True)

# And a corresponding grid
ax.grid(which='both')
ax.grid(which='minor', alpha=0.2)
ax.grid(which='major', alpha=0.5)

plt.grid(True)
# ax = plt.axes()
ax.plot(indexes, survived_repos);
ax.set_xlabel("Threshold (# of Files)",fontsize=15)
ax.set_ylabel("# of Repositories",fontsize=15)
ax.set_xscale('log')
plt.show()


exit()

# exit()
# pl.hist(file_numbers, bins=np.logspace(np.log10(1),np.log10(10000.0), 50), density=True, alpha=0.5,histtype='stepfilled', color='#ff5722',edgecolor='none')
# plt.grid(axis='y', alpha=0.75)
# pl.gca().set_xscale("log")
# pl.show()

# sns.set_style('whitegrid')
# sns.kdeplot(np.array(commit_numbers), bw=0.5)
# pl.show()

# sns.set_style('whitegrid')
# sns.kdeplot(np.array(file_numbers), bw=0.5)
# pl.show()
# #--------------------------------------------------------------------------------
