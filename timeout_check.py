from __future__ import print_function
import sys
import os
import threading
from time import sleep
try:
    import thread
except ImportError:
    import _thread as thread

def quit_function(fn_name):
    # print to stderr, unbuffered in Python 2.
    print('{0} took too long'.format(fn_name), file=sys.stderr)
    sys.stderr.flush() # Python 3 stderr is likely buffered.
    thread.interrupt_main() # raises KeyboardInterrupt
def exit_after(s):
    '''
    use as decorator to exit process if 
    function takes longer than s seconds
    '''
    def outer(fn):
        def inner(*args, **kwargs):
            timer = threading.Timer(s, quit_function, args=[fn.__name__])
            timer.start()
            try:
                result = fn(*args, **kwargs)
            finally:
                timer.cancel()
            return result
        return inner
    return outer

@exit_after(500)
def test():
    import pickle
    import subprocess
    from time import sleep
    import numpy as np 

    f = open('records.pckl', 'rb')
    records=pickle.load(f);
    # exit()
    f = open('repos.pckl', 'rb')
    repos=pickle.load(f);

    record_by_repo = {}
    for key in records.keys():
        for record in records[key]:
            record_by_repo[record['repo']] = record

    repo_info={}
    repos=np.unique(repos)

    for repo in repos:
        try:
            print('Current Repo:'+repo);
            folder_name=repo.replace("/",'-')
            batcmd="mkdir  './repos/{}'".format(folder_name)
            print("mkdir  './repos/{}'".format(folder_name))
            sleep(1)
            os.system(batcmd)
            # init = subprocess.check_output(batcmd, shell=True)
            print("hi")
            subprocess.check_output("git init ./repos/{}".format(folder_name), shell=True) 
            print("git init ./repos/{}".format(folder_name))
            subprocess.check_output("git --git-dir=./repos/{}/.git remote add origin https://github.com/{}".format(folder_name,repo), shell=True)
            subprocess.check_output("git --git-dir=./repos/{}/.git fetch ".format(folder_name), shell=True)
            batcmd="git --git-dir=./repos/{}/.git log origin/master --pretty=oneline --abbrev-commit".format(folder_name)
            commits = subprocess.check_output(batcmd, shell=True).decode("utf-8")
            # print("commits:"+(commits.decode("utf-8")))
            commitsNo = subprocess.check_output("git --git-dir=./repos/{}/.git log origin/master --pretty=%h --abbrev-commit | wc -l".format(folder_name), shell=True).decode("utf-8")
            # print("commitsNo:"+(commitsNo.decode("utf-8")))
            batcmd="svn ls -R https://github.com/{}/branches/master |grep -v '/$' |grep -v '^\.' | wc -l".format(repo)
            filesNo = subprocess.check_output(batcmd, shell=True).decode("utf-8")
            repo_info[repo] = {
                'record':record_by_repo[repo],
                'filesNo':filesNo,
                'commits':commits,
                'commitsNo':commitsNo,
            }
            print(repo_info[repo])
        except Exception as e:
            # print(//"hi"+str(e)); 
            f = open('./repos/error-'+repo.replace('/','_')+'.pckl', 'wb')
            pickle.dump(str(e), f)
            f.close()
            print("error: "+str(e)+" in repo: "+repo)
            continue;
        sleep(0.1);
    f = open('./statistics.pckl', 'wb')
    pickle.dump(repo_info, f)
    f.close()


test()