import pickle
import subprocess
from time import sleep
import numpy as np
from collections import Counter
from itertools import combinations
import matplotlib.pyplot as plt
import scipy.stats as ss
from scipy.stats import gaussian_kde
import seaborn as sns
import pylab as pl
import requests
import sys


# --------------------------------------------------------------------------------
# check validity of repos
# f = open('records.pckl', 'rb')
# records=pickle.load(f);

# f = open('repos.pckl', 'rb')
# repos=pickle.load(f);
# remained_repos=repos
# print(len(remained_repos));

# record_by_repo = {}
# for key in records.keys():
#     for record in records[key]:
#         record_by_repo[record['repo']] = record

# paths ={}
# invalid_pkg=[]
# cnt=0
# howmany=0
# for repos in record_by_repo.keys():
#     cnt+=1
#     # print (str(cnt)+'/'+str(len(record_by_repo.keys())), end="\r")

#     # print(record_by_repo[repos])
#     # exit()
#     # print(record_by_repo[repos]['path'])
#     url = 'https://raw.githubusercontent.com/{}/master/{}'.format(record_by_repo[repos]['repo'],record_by_repo[repos]['path'])
#     # print(url)
#     params = dict(
#         origin='Chicago,IL',
#         destination='Los+Angeles,CA',
#         waypoints='Joplin,MO|Oklahoma+City,OK',
#         sensor='false'
#     )
#     try:
#         resp = requests.get(url=url, params=params)
#         data = resp.json()
#         # print(repos)
#         # print(record_by_repo[repos]['lib'])
#         at_lib='@'+record_by_repo[repos]['lib']
#         if (not('dependencies' in data.keys() and
#                     (record_by_repo[repos]['lib'] in data['dependencies'].keys() or at_lib in data['dependencies'].keys()) )
#             and not('devDependencies' in data.keys() and
#                     (record_by_repo[repos]['lib'] in data['devDependencies'].keys() or at_lib in data['devDependencies'].keys() )    )
#             ):
#             print(repos+'('+record_by_repo[repos]['lib']+','+at_lib+') -> '+record_by_repo[repos]['path'])
#             invalid_pkg.append(repos)
#             print()
#         elif( '/' in record_by_repo[repos]['path']):
#             howmany+=1
#             # print(repos+'('+record_by_repo[repos]['lib']+') -> '+record_by_repo[repos]['path'])
#             # print()
#     except:
#             invalid_pkg.append(repos)
# # print(howmany)
# f = open('./records.pckl', 'wb')
# pickle.dump(records, f)
# f.close()

# print(len(invalid_pkg))

# exit()
# for invalid_repo in invalid_pkg:
#     if invalid_repo in remained_repos:
#         remained_repos.remove(invalid_repo)
# print(len(remained_repos));
# print(remained_repos)
# f = open('./remained_repos.pckl', 'wb')
# pickle.dump(remained_repos, f)
# f.close()

# --------------------------------------------------------------------------------
# f = open('final_retrieved_data.pckl', 'rb')
# records = pickle.load(f)

# commit_obj = {}
# for repo in records:
#     # print(records[repo]['commits'])
#     commits = records[repo]['commits'].strip().split("\n")
#     commit_obj[repo] = []
#     for commit in commits:
#         commit_obj[repo].append({
#             'hash': commit[0:7],
#             'msg': commit[8:]
#         })
#     # if int(len(commit_obj[repo]))!=int(records[repo]['commitsNo']):
#     #     print(repo)
#     #     print(len(commit_obj[repo]))
#     #     print(records[repo]['commitsNo'])
#     # sleep(1)

# threshold = 0.7
# omission_list = []
# # create unique pairs
# repo_list = []
# for repos in records.keys():
#     repo_list.append(repos)
# pair_list = [comb for comb in combinations(repo_list, 2)]
# #--------------------------------------------------------------------------------
# #plot commit number and file number
# commit_numbers = []
# file_numbers = []
# for repo in repo_list:
#     commitNo=int(records[repo]['commitsNo'].strip())
#     commit_numbers.append(commitNo)
#     fileNo=int(records[repo]['filesNo'].strip())
#     file_numbers.append(fileNo)

# # pl.hist(file_numbers, bins=np.logspace(np.log10(1),np.log10(10000.0), 50), density=True, alpha=0.5,histtype='stepfilled', color='#ff5722',edgecolor='none')
# # plt.grid(axis='y', alpha=0.75)
# # pl.gca().set_xscale("log")
# # pl.show()

# sns.set_style('whitegrid')
# sns.kdeplot(np.array(commit_numbers), bw=0.5)
# pl.show()

# sns.set_style('whitegrid')
# sns.kdeplot(np.array(file_numbers), bw=0.5)
# pl.show()
# # #--------------------------------------------------------------------------------

# # exit()
# cnt = 0
# for pair in pair_list:
#     cnt += 1
#     same_commits = 0
#     all_commits = 0
#     repo1 = pair[0]
#     repo2 = pair[1]
#     fork_info = []
#     print(str(cnt)+'/'+str(len(pair_list)) +" - comparing {} and {}:".format(repo1, repo2), end="\r")
#     precentage1 = int(records[repo1]['commitsNo'])
#     precentage2 = int(records[repo2]['commitsNo'])
#     for commits1 in commit_obj[repo1]:
#         for commits2 in commit_obj[repo2]:
#             all_commits += 1
#             if commits1['hash'] == commits2['hash']:
#                 same_commits += 1
#     precentage1 = same_commits/precentage1
#     precentage2 = same_commits/precentage2
#     if(precentage1 > threshold and precentage2 > threshold):
#         omission_list.append([{'repo':repo1,'precentage':precentage1},{'repo':repo2,'precentage':precentage2}])
#         f = open('./fork_detection_result.pckl', 'wb')
#         pickle.dump(omission_list, f)
#         f.close()
#         print(len(omission_list))
#     # print("                                                                                                            ",end="\r")
    
#     # if(precentage1 > threshold and precentage2 > threshold):
#     #     if(records[repo1].commitsNo < records[repo2].commitsNo):
#     #         omission_list.append(repo1)
#     #     else:
#     #         omission_list.append(repo2)
#     # elif(precentage1 > threshold):
#     #     omission_list.append(repo1)
#     # elif(precentage2 > threshold):
#     #     omission_list.append(repo2)
#     # print(omission_list)
#     # print()
#     # sleep(1)
# f = open('./fork_detection_result.pckl', 'wb')
# pickle.dump(omission_list, f)
# f.close()

# for forked_repo in omission_list:
#     del repos[forked_repo]

# f = open('./records.pckl', 'wb')
# pickle.dump(records, f)
# f.close()
#-----------------------------------------------------------------------------
#finding the pairs for ommission
f = open('fork_detection_result.pckl', 'rb')
results = pickle.load(f)
ommission_repo=[]
for forked_pair in results:
    if forked_pair[0]['precentage'] >forked_pair[1]['precentage']:
        ommission_repo.append(forked_pair[0]['repo'])
    else: #if forked_pair[0]['percentage'] < forked_pair[1]['percentage']:
        ommission_repo.append(forked_pair[1]['repo'])
# print(len(ommission_repo))


f = open('final_retrieved_data.pckl', 'rb')
final_records = pickle.load(f)
# for repo in final_records:
#     print(final_records[repo])
for repo in ommission_repo:
    if(repo in final_records.keys()):
        del final_records[repo]
    else:
        print(repo)
print(len(final_records))
f = open('./after_fork_detection_filter.pckl', 'wb')
pickle.dump(final_records, f)
f.close()

