import pickle
from os import listdir
from os.path import isfile, join
import numpy as np 

mypath="./errors/"
onlyfiles = [f for f in listdir(mypath) if isfile(join(mypath, f))]
errors =[]
repos = []
for file in onlyfiles:
    path = join(mypath,file)
    repo=file.replace("error-",'').replace(".pckl",'').replace("_",'/')
    f = open(path, 'rb')
    error=pickle.load(f);
    errors.append(error)
    repos.append(repo)
#here we have repos and errors!

f = open('statistics.pckl', 'rb')
repo_info=pickle.load(f);
retrieved_repos = np.array(list(repo_info.keys()));
f = open('init_repos.pckl', 'rb')
all_repos=pickle.load(f)
all_repos=np.unique(all_repos)
for repo in retrieved_repos:
    all_repos = np.delete(all_repos,np.where(all_repos == repo));
print(len(all_repos))
print(len(repos))

# for repo in all_repos:
#     repos = np.delete(repos,np.where(repos==repo))
f = open('./repos.pckl', 'wb')
pickle.dump(repos, f)
f.close()

# print(repos)
# print(len(all_repos))    
# uniquenp.unique(errors)
# print(error);
# print()
# print()
# print()

