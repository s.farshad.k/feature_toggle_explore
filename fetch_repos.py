import sqlite3
from selenium import webdriver 
from selenium.webdriver.common.by import By 
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC 
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.expected_conditions import presence_of_element_located
import os
import sys
import numpy as np
import time
import pandas as pd
import pickle
import urllib.parse
import random
import psutil

libList = [
  'textlint/feature-flag',
  'babel-plugin-feature-flags',
  'ember-feature-flags',
  'flopflip',
  'fflip',
  'appknobs',
  'lwc/features',
  'angular-feature-flags',
  'rollouter',
  'configcat-js',
  'ngx-feature-toggle',
  'flagger',
  'ld-react',
  'react-feature-flags',
  'opticks',
  'bullet-train-client',
  'ldclient-node',
  'ldclient-js',
  'ldclient-node-dynamodb-store',
  'ldclient-react',
  'launchdarkly-node-client-sdk',
  'launchdarkly-js-sdk-common',
  'launchdarkly-js-client-sdk',
  'launchdarkly-react-client-sdk',
  'launchdarkly-node-server-sdk',
  'launchdarkly-js-test-helpers',
];
random.shuffle(libList)

timeout=10

try:
    f = open('loginDriverInfo.pckl', 'rb')
    driverInfo=pickle.load(f);
    driver = webdriver.Remote(command_executor=driverInfo['executor_url'], desired_capabilities={})
    print(driver)
    driver_process = psutil.Process(driver.service.process.pid)
    if driver_process.is_running():
        raise ValueError('Browser does not exist.')

except:
    os.system('python ./login.py')
    f = open('loginDriverInfo.pckl', 'rb')
    driverInfo=pickle.load(f);
    driver = webdriver.Remote(command_executor=driverInfo['executor_url'], desired_capabilities={})
    
driver.session_id = driverInfo['session_id']
wait = WebDriverWait(driver, timeout)
repos=[];
records={}
for lib in libList:
    page=1
    max_page = 100
    startTime=time.time()
    records[lib]=[]
    while page<=max_page :
        search_addr='https://github.com/search?l=JSON&p={}&q='.format(page)+urllib.parse.quote(lib)+'+filename%3Apackage.json&type=Code';
        # print(search_addr)
        driver.get(search_addr)
        try:
            first_result = wait.until(presence_of_element_located((By.CSS_SELECTOR, ".d-flex>h3")))
        except:
            max_page=-1
            print('No result for library {}. Going for next one...'.format(lib))

        if(page==1):
            try:
                resultsNo=int(first_result.get_attribute('textContent').lower().replace('showing','').strip().split(' ')[0])
                max_page = int(resultsNo/10)+1
                print('{} results are found for lib {}. They will be shown in {} pages.'.format(resultsNo, lib,max_page))
            except:
                print('No result for library {}. Going for next one...'.format(lib))
                max_page=-1
                continue

        # elements = driver.find_elements_by_css_selector(".flex-shrink-0>.link-gray")
        elements = driver.find_elements_by_css_selector('.code-list-item-public .width-full');
        for element in elements:
            repos.append(element.find_element_by_css_selector('.flex-shrink-0 ').get_attribute('textContent').strip())
            records[lib].append({
                'repo':element.find_element_by_css_selector('.flex-shrink-0 ').get_attribute('textContent').strip(),
                'path':element.find_element_by_css_selector('.f4').get_attribute('textContent').strip(),
                'lib':lib,
            })
        time.sleep(0.2)
        page+=1
    print('library {} finished, took {} time. It has {} search results.'.format(lib,time.time()-startTime,len(records[lib])))
    time.sleep(2)
records_no=0
for lib in records.keys():
    records_no+=len(records[lib])
    print('{}:{}'.format(lib,len(records[lib])))
print(records_no)
print(len(repos))
print(len(np.unique(repos)))
f = open('./repos.pckl', 'wb')
pickle.dump(repos, f)
f.close()
f = open('./records.pckl', 'wb')
pickle.dump(records, f)
f.close()


