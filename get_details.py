import requests
import pickle
from time import sleep
from selenium import webdriver 
from selenium.webdriver.common.by import By 
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC 
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.expected_conditions import presence_of_element_located
import json
import io

repo_list = [
    "51ngul4r1ty/atoll-core ",
    "BulletTrainHQ/bullet-train-frontend",
    "RCOSDP/RDM-ember-osf-web",
    "aptible/dashboard.aptible.com",
    "aymerick/kowa-client",
    "bcgov/ppr",
    "cds-snc/ircc-rescheduler",
    "codebyravi/ember-library",
    "datacite/bracco",
    "ddicorpo/RecruitInc",
    "dtacci/launchdarkly-test",
    "ilios/common",
    "krispaks/HSEmployee",
    "lblod/ember-rdfa-editor",
    "lblod/frontend-gelinkt-notuleren",
    "lchjczw/gitter_webapp",
    "newjersey/career-network",
    "slolobdill44/Hamcamp",
    "vitorhariel/monitory",
    "bcgov/bcrs-business-create-ui",
    "chpladmin/chpl-website",
    "cstffx/atlaskit2",
    "hmcts/rpx-xui-common-lib",
    "ibroadfo/flowerpot",
    "innovate-technologies/control",
    "onap/vid",
    "sarupbanskota/get-yalla",
    "severinbeauvais/sbc-common-components",
    "travis-ci/travis-web",
    "xaasutility/xsu",
]

unfiltered_repos=[
	"51ngul4r1ty/atoll-core",
	"51ngul4r1ty/atoll-shared",
	"BulletTrainHQ/bullet-train-frontend",
	"CenterForOpenScience/ember-osf",
	"RCOSDP/RDM-ember-osf-web",
	"UpdraftGroup/CapitolZen-Frontend",
	"aptible/dashboard.aptible.com",
	"arnaudlimbourg/formidable-ui",
	"aymerick/kowa-client",
	"bcgov/ppr",
	"cds-snc/ircc-rescheduler",
	"codebyravi/ember-library",
	"datacite/bracco",
	"ddicorpo/RecruitInc",
	"dtacci/launchdarkly-test",
	"forestryio/forestry.io",
	"hmcts/rpx-xui-manage-organisations",
	"hmcts/rpx-xui-webapp",
	"ilios/common",
	"krispaks/HSEmployee",
	"lblod/ember-rdfa-editor",
	"lblod/frontend-gelinkt-notuleren",
	"lchjczw/gitter_webapp",
	"newjersey/career-network",
	"nypublicradio/wnyc-web-client",
	"nypublicradio/wqxr-web-client",
	"slolobdill44/Hamcamp",
	"textlint/textlint",
	"vitorhariel/monitory",
	"zotoio/github-task-manager",
	"bcgov/bcrs-business-create-ui",
	"chpladmin/chpl-website",
	"cstffx/atlaskit2",
	"hmcts/rpx-xui-common-lib",
	"ibroadfo/flowerpot",
	"innovate-technologies/control",
	"onap/vid",
	"sarupbanskota/get-yalla",
	"severinbeauvais/sbc-common-components",
	"streamlink/streamlink-twitch-gui",
	"travis-ci/travis-web",
	"xaasutility/xsu",
]
# from github import Github
# driver = webdriver.Chrome(executable_path="C:/Users/farshad/Desktop/chromedriver.exe")

# login_time_out = 10
# wait = WebDriverWait(driver, login_time_out)
# #login
# # for repo in repo_list:
# for repo in unfiltered_repos:
#     try:
#         driver.get('https://api.github.com/repos/{}'.format(repo))
#         # sleep(1)
#         content = driver.find_element_by_tag_name('pre').text
#         folder_name=repo.replace("/",'-')
#         with io.open('./repo_detail/{}.json'.format(folder_name.strip()), "w", encoding="utf-8") as f:
#             f.write(content)
#         # f = open('./repo_detail/{}.json'.format(folder_name.strip()), 'w')
#         # # pickle.dump(content, f)
#         # f.write(content)
#         # f.close()
#         sleep(2)
#     except:
#         print(repo)
#     # json_data = json.loads(content)
#     # print(json_data)
#     # now check the limitations to see if we have any more request left
#     # driver.get('https://api.github.com/rate_limit')
#     # sleep(1)
#     # content = driver.find_element_by_tag_name('pre').text
#     # json_data = json.loads(content)
#     # print((json_data['resources']['core']['remaining']))
#     # sleep(2)
# driver.close()

# # ft = True
# # print(len(repo_list))
# # for repo in repo_list:
# #     url = "https://api.github.com/repos/{}".format(repo)
# #     print(url)
# #     response = requests.get(url,
# #                             headers={
# #                                 'Content-Type': 'application/json',
# #                                 # 'Authorization': 'token %s' % token,
# #                             }
# #                             )
# #     print(response.json())
# #     exit()
# #     # if ft:
# #     #     ft=False
# #     #     print(response);
# #     # folder_name=repo.replace("/",'-')
# #     # f = open('./repo_detail/{}'.format(folder_name), 'wb')
# #     # pickle.dump(response, f)
# #     # f.close()
# #     # print(repo+" data is retrieved!");
# #     # sleep(1)


# #--------------------------------------
# # Project sizes
licenses = {}
for repo in unfiltered_repos:
    folder_name=repo.replace("/",'-')
    file = open('./repo_detail/{}.json'.format(folder_name.strip()),mode='r')
    all_of_it = file.read()
    file.close() 
    pkg = json.loads(all_of_it)
    try:
        if pkg['license']['key'] in licenses.keys():
            licenses[pkg['license']['key']]+=1
        else:
            licenses[pkg['license']['key']]=1
    except:
        if "none" in licenses.keys():
            licenses["none"]+=1
        else:
            licenses["none"]=1
    # if pkg['license']['key'] in licenses.keys():
    #     licenses[pkg['license']['key']]+=1
    # else:
    #     licenses[pkg['license']['key']]=1
print(licenses)